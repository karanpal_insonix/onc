-- MySQL dump 10.13  Distrib 5.7.9, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: doconcall
-- ------------------------------------------------------
-- Server version	5.7.10-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `appointment`
--

DROP TABLE IF EXISTS `appointment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `appointment` (
  `id` bigint(45) NOT NULL AUTO_INCREMENT,
  `patient_id` varchar(45) NOT NULL,
  `doctor_id` varchar(45) NOT NULL,
  `schedule_id` varchar(45) NOT NULL,
  `reason` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `appointment`
--

LOCK TABLES `appointment` WRITE;
/*!40000 ALTER TABLE `appointment` DISABLE KEYS */;
/*!40000 ALTER TABLE `appointment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `doctor`
--

DROP TABLE IF EXISTS `doctor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `doctor` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `license_number` varchar(45) NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `speciality_id` bigint(20) DEFAULT NULL,
  `location_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id_idx` (`user_id`),
  KEY `location_id_idx` (`location_id`),
  KEY `speciality_id_idx` (`speciality_id`),
  CONSTRAINT `location_id` FOREIGN KEY (`location_id`) REFERENCES `location` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `speciality_id` FOREIGN KEY (`speciality_id`) REFERENCES `speciality` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `doctor`
--

LOCK TABLES `doctor` WRITE;
/*!40000 ALTER TABLE `doctor` DISABLE KEYS */;
INSERT INTO `doctor` VALUES (1,'Harpreet','singh','123123',1,3,1),(2,'Ravi','Garg','345345',2,3,2),(3,'Dr','Walia','112233',3,3,3),(4,'Dr','Garg','234123',4,3,4),(5,'Rajat','Sharma','676544',5,2,1),(6,'Alka','Verma','321212',6,2,2),(7,'Neelam','Sharma','987876',7,2,3),(8,'Dr','Sakshi','367654',8,2,4),(9,'Neha','Dogra','923451',9,1,1),(10,'Dr','Kuldeep','598215',10,1,2),(11,'Hemant','Verma','323244',11,1,3),(12,'Gorav','Singla','198611',12,1,4),(13,'Sarah','Khan','213564',13,4,1),(14,'Heena','Koshal','432123',14,4,2),(15,'Amandeep','kaur','786123',15,4,3),(16,'Kapoor','Chand','M34352',16,4,4),(17,'Rimpi','Sharma','W32E23',17,5,1),(18,'Jashan','Kaur','U437861',18,5,2),(19,'Sandeep','Kaur','S326598',19,5,3),(20,'Arman','Thakur','A652986',20,5,4),(21,'Suresh','Chug','S878233',21,6,1),(22,'Amrit','Kaur','3246510',22,6,3),(23,'Miss','Arsh','M382761',23,6,4),(24,'Yogesh','Kaushil','9007642',24,7,1),(25,'Rajan','Singh','0006321',25,7,4),(26,'Jagmohan','Virk','V987620',26,7,2),(27,'Meghna','Walia','0098231',27,8,2),(28,'Dr ','Neeraj','N230935',28,8,1),(29,'Atin','Sood','090403',29,8,3),(30,'Sanjay','Bangar','B34T56',30,8,4),(31,'Gurinder','Singh','113567',31,9,1),(32,'Miss','Kanika','27642',32,9,2),(33,'Miss ','Sunena','009956',33,9,3),(34,'Tanvi','Chabra','21230',34,10,2),(35,'Miss ','Monica','909021',35,10,3),(36,'Naina','Singh','N340I76',36,10,4),(37,'Rakesh','Chandan','R110076',37,10,4),(38,'Dr','Nanda','4433517',38,11,1),(39,'Dr','Bajwa','2N345M',39,11,2),(40,'Navpreet','Kaur','L785468',40,11,2),(41,'Miss','Kamal','309023',41,11,3),(42,'Jagjit','Singh','768765',42,12,2),(43,'Sarah','Kapoor','999878',43,12,4),(44,'Ranjit','Singh','R431009',44,12,3),(45,'Vinod ','Khanna','333421',45,12,4),(46,'Rimpy','Kaushal','00009',46,1,4),(47,'Vinay','Toor','878898',47,2,3),(48,'Arun','Thakral','12786',48,3,2),(49,'Dharamjit','Singh','00865',49,4,1),(50,'Harkaran','Singh','88779',50,5,1),(51,'Sidharth','Thakur','96824',51,6,2),(52,'Daljit','Singh','556613',52,7,3),(53,'Gaytri','Sood','G7872G2',53,4,2),(54,'Kalpna','Chawla','635424',54,3,2),(55,'Reena','Sharma','R82372',55,2,1);
/*!40000 ALTER TABLE `doctor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `location`
--

DROP TABLE IF EXISTS `location`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `location` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `sector` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `clinic_name` varchar(255) NOT NULL,
  `pin_no` int(11) NOT NULL,
  `doctor_id` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `location`
--

LOCK TABLES `location` WRITE;
/*!40000 ALTER TABLE `location` DISABLE KEYS */;
INSERT INTO `location` VALUES (1,'Sector-22A','Chandigarh','Medicos Clinic',160022,'9'),(2,'Sector-10','Mohali','Family Clinic',140110,'10'),(3,'Sector-4','Panchkula','Heart Clinic',134112,'11'),(4,'Sarhind road','Patiala',' Heart Care Centre',160022,'12'),(5,'Sector 18-A','Chandigarh','Advanced Dental',160018,'5'),(6,'Sector 60','Mohali','Dental Excellence',140301,'6'),(7,'Sector 8','Panchkula','Smile clinic',160009,'7'),(8,'Phatak','Patiala','Noor Dental Clinic',147001,'8'),(9,'Sector 35C','Chandigarh','Perfect Smile Dental Clinic',160022,'55'),(10,'Sector 71','Chandigarh','Skin Clinic',160071,'1');
/*!40000 ALTER TABLE `location` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `patient`
--

DROP TABLE IF EXISTS `patient`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `patient` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(45) NOT NULL,
  `last_name` varchar(45) NOT NULL,
  `dob` varchar(45) NOT NULL,
  `gender` varchar(45) NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  KEY `userid_idx` (`id`),
  CONSTRAINT `userid` FOREIGN KEY (`id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `patient`
--

LOCK TABLES `patient` WRITE;
/*!40000 ALTER TABLE `patient` DISABLE KEYS */;
INSERT INTO `patient` VALUES (1,'Aman','deep','20 jan 2000','female',56),(2,'Harmeet','Kaur','4.12.2000','female',57),(3,'Jaspreet','Kaur','23-02-02','female',58),(4,'Jyoti ','Kapoor','13/12/2014','female',6),(5,'cc','cc','28/04/1986','male',7),(6,'w','w','13/12/2014','male',8);
/*!40000 ALTER TABLE `patient` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `schedule`
--

DROP TABLE IF EXISTS `schedule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `schedule` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `date` varchar(45) NOT NULL,
  `time` varchar(45) NOT NULL,
  `filled` varchar(45) NOT NULL,
  `doctor_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `id` FOREIGN KEY (`id`) REFERENCES `doctor` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `schedule`
--

LOCK TABLES `schedule` WRITE;
/*!40000 ALTER TABLE `schedule` DISABLE KEYS */;
INSERT INTO `schedule` VALUES (1,'17-05-2016','9:00am','false',9),(2,'17-05-2016','9:30am','false',9),(3,'17-05-2016','10:00am','false',9),(4,'17-05-2016','10:30am','false',9),(5,'17-05-2016','11:00am','false',9),(6,'17-06-2016','11:30am','false',9),(7,'17-06-2016','12:00pm','false',9),(8,'17-06-2016','12:30pm','false',9),(9,'17-06-2016','1:00pm','false',9),(10,'17-06-2016','1:30pm','false',9),(11,'17-06-2016','2:00pm','false',9),(41,'17-06-2016','9:00am','false',12),(42,'17-06-2016','9:30am','false',12),(43,'17-06-2016','10:00am','false',12),(44,'17-06-2016','10:30am','false',12),(45,'17-06-2016','11:00am','false',12),(46,'17-06-2016','11:30am','false',12),(47,'17-06-2016','12:00pm','false',12),(48,'17-06-2016','9:00am','false',8),(49,'17-06-2016','9:30am','false',8),(50,'17-06-2016','10:00am','false',8),(51,'17-06-2016','10:30am','false',8),(52,'17-06-2016','11:00am','false',8),(53,'17-06-2016','11:30am','false',8),(54,'17-06-2016','12:00pm','false',8),(55,'17-06-2016','9:00am','false',11);
/*!40000 ALTER TABLE `schedule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `speciality`
--

DROP TABLE IF EXISTS `speciality`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `speciality` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `speciality`
--

LOCK TABLES `speciality` WRITE;
/*!40000 ALTER TABLE `speciality` DISABLE KEYS */;
INSERT INTO `speciality` VALUES (1,'Cardiologist'),(2,'Dentist'),(3,'Dermatologist'),(4,'ENT'),(5,'Eye Care'),(6,'Gastroenterology'),(7,'Gynecologist'),(8,'Immunologist'),(9,'Neurologist'),(10,'Pediatrics'),(11,'Surgical Specialist'),(12,'Others');
/*!40000 ALTER TABLE `speciality` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `email` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=78 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'DrHarpreet@gmail.com','111'),(2,'DrRav@gmail.com','111'),(3,'Drwalia@gmail.com','111'),(4,'Drgarg@gmail.com','111'),(5,'DrRajat@gmail.com','123'),(6,'DrAlka@gmail.com','123'),(7,'DrNeelam@gmail.com','111'),(8,'Drsakshi@gmail.co','111'),(9,'DrNeha@gmail.com','111'),(10,'DrKuldeep@gmail.com','111'),(11,'DrHemant@gmail.com','111'),(12,'DrGorav@gmail.com','111'),(13,'MsSarah@gmail.com','111'),(14,'MsHeena@gmail.com','111'),(15,'MsAman@gmail.com','111'),(16,'DrKapoor@gmail.com','111'),(17,'DrRimpi@gmail.com','111'),(18,'MissJashan@gmail.com','111'),(19,'MsSandeep@gmail.com','111'),(20,'DrArman@gmail.com','111'),(21,'DrSuresh@gmail.com','111'),(22,'DrAmrit@gmail.com','111'),(23,'MsArsh@gmail.com','111'),(24,'DrYogesh@gmail.com','111'),(25,'DrRajan@gmail.com','111'),(26,'DrJagmohan@gmail.com','111'),(27,'MsMeghna@gmail.com','111'),(28,'DrNeeraj@gmail.com','111'),(29,'DrAtin@gmail.com','111'),(30,'DrSanjay@gmail.com','111'),(31,'DrGurinder@gmail.com','111'),(32,'MsKanika@gmail.com','111'),(33,'MsSunena@gmail.com','111'),(34,'DrTanvi@gmail.com','111'),(35,'MissMonica@gmail.com','111'),(36,'MsNaina@gmail.com','111'),(37,'DrRakesh@gmail.com','111'),(38,'DrNanda@gmail.com','111'),(39,'DrBajwa@gmail.com','111'),(40,'MsNavpreet@gmail.com','111'),(41,'MsKamal@gmail.com','111'),(42,'DrJagjit@gmail.com','111'),(43,'DrSarah@gmail.com','111'),(44,'DrRanjit@gmail.com','111'),(45,'DrVinod@gmail.com','111'),(46,'MsRimpy@gmail.com','111'),(47,'DrVinay@gmail.com','111'),(48,'DrArun@gmail.com','111'),(49,'DrDharam@gmail.com','111'),(50,'DrHarkaran@gmail.com','111'),(51,'DrSid@gmail.com','111'),(52,'DrDaljit@gmail.com','111'),(53,'DrGatyri@gmail.com','111'),(54,'MsKalpna','111'),(55,'Dr MsReena','111');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-05-17 11:23:31
