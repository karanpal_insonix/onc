<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet" href="resources/css/bootstrap.css">
<link rel="stylesheet" href="resources/css/datepicker.min.css" />
<link rel="stylesheet" href="resources/css/datepicker3.min.css" />
<link rel="stylesheet" href="resources/css/prism.css" />

<title>Book your Appointment</title>
</head>
<body background="resources/img/signup11.jpg" style="background-size:cover; background-repeat:no-repeat"">
	<%@include file="template/header.jsp"%>
	<div class="container" class="clr">
		<h3 style="font-family: Lucida Calligraphy;">
			<font color="orange"><i>Book Your Appointment Here</i> </font>
		</h3>
		<div class="row" style="color: #339966">

			<div class="col-sm-6 margin_div" style="border: 2px solid orange">
				<form class="form-horizontal" name="appointment"
					action="bookAppointment" id="signup" method="post">
					<input type="hidden" name="patientId" value="${patient.id }">
					<input type="hidden" name="doctorId" value="${doctor.id }">
					<div class="form-group">
						<label for="first_name" class="col-sm-3 control-label">FIRST
							NAME</label>
						<div class="col-sm-8">
							<input type="text" class="form-control" id="first_name"
								value="${patient.firstname}" name="firstName">
						</div>
					</div>

					<div class="form-group">
						<label for="last_name" class="col-sm-3 control-label">LAST
							NAME</label>
						<div class="col-sm-8">
							<input type="text" class="form-control" id="last_name"
								name="lastName" value="${patient.lastname }">
						</div>
					</div>

					<div class="form-group">
						<label class="col-xs-3 control-label">Date</label>
						<div class="col-xs-5 date">
							<div class="input-group input-append date" id="date">
								<input type="text" class="form-control" name="date" /> <span
									class="input-group-addon add-on"><span
									class="glyphicon glyphicon-calendar"></span></span>
							</div>
						</div>
					</div>

					<div class="form-group ">
						<label class="col-xs-3 control-label" for="time"> Time </label>
						<div class="col-xs-5 date">
							<select class="select form-control" id="time" name="time">
								<c:forEach var="schedule" items="${scheduleList }">
									<option value="${schedule.id}">${schedule.time}</option>
								</c:forEach>
							</select>
						</div>
					</div>

					<div class="form-group">
						<label for="last_name" class="col-sm-3 control-label">Illness</label>
						<div class="col-sm-8">
							<input type="text" name="illness" class="form-control"
								id="illness">
						</div>
					</div>


					<div class="form-group">
						<label for="appointment" class="col-sm-3 control-label">Appointment</label>
						<div class="col-sm-8">
							<div class="radio">
								<label><input type="radio" name="appointment"
									id="appointment" value="yourself" checked="true">For
									Yourself</label> <label><input type="radio" name="appointment"
									id="appointment" value="Someoneelse">For Someone else</label>
							</div>
						</div>
					</div>

					<div id="next_form" style="display: none">

						<!-- Add fields  -->

						<div class="form-group">
							<label for="pRelativeFirstName" class="col-sm-3 control-label">Patient
								First Name </label>
							<div class="col-sm-8">
								<input type="text" class="form-control"
									name="pRelativeFirstName" id="pRelativeFirstName">
							</div>
						</div>

						<div class="form-group">
							<label for="pRelativeLastName" class="col-sm-3 control-label">Patient
								Last Name</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" name="pRelativeLastName"
									id="pRelativeLastName">
							</div>
						</div>
						<div class="form-group">
							<label for="gender" class="col-sm-3 control-label">Gender</label>
							<div class="col-sm-8">
								<div class="radio">
									<label><input type="radio" name="gender" id="gender"
										value="male">Male</label> <label><input type="radio"
										name="gender" id="gender" value="female">Female</label>
								</div>
							</div>
						</div>
					</div>
					<div class="form-group">
						<div>
							<button class="btn btn-primary " name="submit" type="submit">
								Submit</button>
						</div>
					</div>
				</form>
			</div>
		</div>

	</div>
	<%@include file="template/footer.jsp"%>
	<script src="resources/js/jquery-1.12.2.js"></script>
	<script src="resources/js/bootstrap-datepicker.min.js"></script>
	<script>
		$(document).ready(function() {
			$('#date').datepicker({
				format : 'mm/dd/yyyy'
			}).on('changeDate', function(e) {
				// Revalidate the date field
				//$('#eventForm').formValidation('revalidateField', 'date');
			});
		});

		$(document).ready(function() {
			$("input[name=appointment]").on("change", function() {
				if ($(this).val() === "yourself") {
					$("#next_form").hide();
				} else {
					$("#next_form").show();
				}
			});
		});
	</script>
</body>
</html>