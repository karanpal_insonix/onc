 <%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
 <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/bootstrap.css">
 
 <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.min.css" />
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker3.min.css" />
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.3/jquery.min.js"></script>

<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.min.js"></script>

<style type="text/css">
/**
 * Override feedback icon position
 * See http://formvalidation.io/examples/adjusting-feedback-icon-position/
 */
#eventForm .form-control-feedback {
    top: 0;
    right: -15px;
}
</style>
<title>Appointment Booked</title>
</head>
<body>
<body background="resources/img/signup1.jpg" style="background-size:cover; background-repeat:no-repeat;">
<%@include file="template/header.jsp" %>
<div style="height:50px">
 <h4><b>Your appointment has been booked. Details are as follows</b></h4></div>
<div>
 <center>
 <div style="margin-bottom:150px"> 
 <table style="width:350px" height="200px" > 
 	<tr>
 		<td>Patient Name</td>
 		<td>${appointment.patient.firstname } ${appointment.patient.lastname }</td> 		
 	</tr>
 	<tr>
 		<td>Doctor Name</td>
 		<td>${appointment.doctor.firstName } ${appointment.doctor.lastName }</td>
 	</tr>
 	<tr>
 		<td>Date</td>
 		<td>${appointment.schedule.date }</td>
 		</tr>
 		<tr> 
 		<td>Time</td>
 		<td>${appointment.schedule.time }</td> 		
 	</tr>
 	
 <tr>
 <td colspan="2">Address</td>
 <td>City</td><td>${appointment.doctor.city}</td>
 <td>Sector</td><td>${appointment.doctor.sector}</td>
 <td>Clinic Name</td><td>${appointment.doctor.clinicName}</td>
 <td>Pin</td><td>${appointment.doctor.pinNo}</td> 
 </tr> 
 </table>
 </div>
 </center>
</div>
 <%@include file="template/footer.jsp" %>
</body>
</html>