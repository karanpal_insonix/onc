<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Doctor Sign Up</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" href="resources/css/bootstrap.css">
<link rel="stylesheet" href="resources/css/datepicker.min.css" />
<link rel="stylesheet" href="resources/css/datepicker3.min.css" />
<link rel="stylesheet" href="resources/css/prism.css" />
<link rel="stylesheet" href="resources/css/intlTelInput.css" />
<style>
.iti-flag {
	background-image: url("resources/img/flags.png");
}

#error-msg {
	color: red;
}

#valid-msg {
	color: #00C900;
}

input.error {
	border: 1px solid #FF7C7C;
}

.error {
	color: red;
}
</style>
</head>
<body background="resources/img/signup11.jpg"
	style="background-size: cover; background-repeat: no-repeat;">
	<%@include file="template/header.jsp"%>


	<div class="container">
		<center>
			<h3 style="font-family: Lucida Calligraphy;">
				<font color="blue"><i><u>Doctor Registration Form</u></i>
			</h3>
		</center>
		<div class="row" style="color: green">
			<div class="col-sm-2"></div>
			<div class="col-sm-8">
				<form class="form-horizontal" name="sign-up"
					action="doctorRegistrationServlet" id="signup" method="post">
					<div class="form-group">
						<label for="emailid" class="col-sm-2 control-label">Email</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="emailid" name="email"
								required="required" placeholder="Type your Email id">
						</div>
					</div>

					<div class="form-group">
						<label for="password" class="col-sm-2 control-label">Password</label>
						<div class="col-sm-10">
							<input type="password" class="form-control" id="password"
								name="password" required="required"
								placeholder="Minimum 8 character">
						</div>
					</div>

					<div class="form-group">
						<label for="cnpass" class="col-sm-2 control-label">Confirm
							Password</label>
						<div class="col-sm-10">
							<input type="password" class="form-control" id="cnpass"
								name="cnpass" required="required" placeholder="Re-Type password">
						</div>
					</div>

					<div class="form-group">
						<label for="fname" class="col-sm-2 control-label">First
							Name</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="fname" value=""
								name="fname" required="required"
								placeholder=" FirstName Required ">
						</div>
					</div>

					<div class="form-group">
						<label for="lname" class="col-sm-2 control-label">Last
							Name</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="lname" value=""
								name="lname" placeholder="Optional"" >
						</div>
					</div>



					<div class="form-group">
						<label for="gender" class="col-sm-2 control-label">Gender</label>
						<div class="col-sm-10">
							<div class="radio">
								<label><input type="radio" name="gender" value="male">Male</label>
								<label><input type="radio" name="gender" value="female">Female</label>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label for="lno" class="col-sm-2 control-label">LiscenceNo.</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="lno" value=""
								name="lno" required="required"
								placeholder="Liscence No. Required">
						</div>
					</div>
					<div class="form-group">
						<label for="Speciality" class="col-sm-2 control-label">Speciality</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="speciality" value=""
								name="speciality" required="required"
								Placeholder="Your Specialisation">
						</div>
					</div>

					<div class="form-group">
						<label for="sector" class="col-sm-2 control-label">Sector</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="sector" value=""
								name="sector" Placeholder="Optional">
						</div>
					</div>
					<div class="form-group">
						<label for="city" class="col-sm-2 control-label">City</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="city" value=""
								name="city" placeholder="Your Home Town">
						</div>
					</div>



					<div class="form-group">
						<label for="phone" class="col-sm-2 control-label">MobileNo</label>
						<div class="col-sm-10">
							<input id="phone" class="form-control" type="tel"> <span
								id="valid-msg" class="hide">Valid</span> <span id="error-msg"
								class="hide">Invalid number</span>
						</div>
					</div>



					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-10">
							<button type="submit" class="btn btn-primary">Sign in</button>
							<input type="reset" value=" Reset All " name="B5"
								class="btn btn-primary">
						</div>
					</div>
				</form>
			</div>
			<div class="col-sm-2"></div>
		</div>
	</div>
	<%@include file="template/footer.jsp"%>

	<script src="resources/js/prism.js"></script>
	<script src="resources/js/jquery-1.12.2.js"></script>
	<script src="resources/js/intlTelInput.js"></script>
	<script src="resources/js/jquery.validate.js"></script>
	<script src="resources/js/additional-methods.js"></script>
	<!-- GOOGLE ANALYTICS -->
	<script type="text/javascript" async="" src="resources/js/ga.js"></script>
	<script>
		var _gaq = _gaq || [];
		_gaq.push([ '_setAccount', 'UA-26178728-1' ]);
		_gaq.push([ '_trackPageview' ]);
		(function() {
			var ga = document.createElement('script');
			ga.type = 'text/javascript';
			ga.async = true;
			ga.src = ('https:' == document.location.protocol ? 'https://ssl'
					: 'http://www')
					+ '.google-analytics.com/ga.js';
			var s = document.getElementsByTagName('script')[0];
			s.parentNode.insertBefore(ga, s);
		})();
	</script>
	<script>
		var telInput = $("#phone"), errorMsg = $("#error-msg"), validMsg = $("#valid-msg");

		// initialise plugin
		telInput.intlTelInput();

		var reset = function() {
			telInput.removeClass("error");
			errorMsg.addClass("hide");
			validMsg.addClass("hide");
		};

		// on blur: validate
		telInput.blur(function() {
			reset();
			if ($.trim(telInput.val())) {
				if (telInput.intlTelInput("isValidNumber")) {
					validMsg.removeClass("hide");
				} else {
					telInput.addClass("error");
					errorMsg.removeClass("hide");
				}
			}
		});

		// on keyup / change flag: reset
		telInput.on("keyup change", reset);

		/*--------------------      form validation  ------------------------*/
		$("#signup").validate({
			rules : {
				"email" : {
					required : true,
					email : true
				},
				"password" : {
					required : true,
					minlength : 8
				},
				"cnpass" : {
					required : true,
					minlength : 8,
					equalTo : "#password"
				},
				"fname" : {
					required : true,
					myField : {
						lettersonly : true
					}
				},
				"lname" : {
					required : true,
					myField : {
						lettersonly : true
					}
				},

				"gender" : {
					required : true
				},
				"lno" : {
					required : true
				},
				"Speciality" : {
					required : true,
				},

				"phone" : {
					required : true
				}
			},
			submitHandler : function(form) {
				// do other things for a valid form
				form.submit();
			}
		});

		/*-------------------- end of form validation  ------------------------*/
	</script>

</body>
</html>