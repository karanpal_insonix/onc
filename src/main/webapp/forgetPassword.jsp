<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" href="resources/css/bootstrap.css">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<title>Forgot Password</title>
</head>


<body background="resources/img/signup11.jpg">
<%@include file="template/header.jsp"%>
<div style="height:350px"> 
	<center>
		<h1 style="font-family: Franklin Gothic Heavy;">
			<font color="#009933"><i>Forgot Password</i>
		</h1>
	</center>
	<form action="./forgetPassword" method="POST">
		<center>
			<div>
				<label for="email">Email Id</label> <input type="text" name="email"
					id="email" />
					<c:if test="${not empty errorMessage }">
					
              <a href="#"><font color="red" style="font-family:Lucida Handwriting font; size:30px">${errorMessage }</font></a>
        	    
              </c:if>
              <c:if test="${not empty message}">
              	 <a href="#"><font color="red" style="font-family:Lucida Handwriting font; size:30px">${message }</font></a>
              </c:if>
					
					
					 <input type="submit" class="btn btn-primary"
					value="Send Verification Link" />
					
					
			</div>
		</center>
		<div></div>
	</form></div>
<%@include file="template/footer.jsp"%>

</body>
</html>