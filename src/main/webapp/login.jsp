<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="resources/css/bootstrap.css">

<title>Login Page</title>
</head>
<body>
<body background="resources/img/signup11.jpg"
	style="background-size: cover; background-repeat: no-repeat;">
	<%@include file="template/header.jsp"%>

	<div class="container" Style="height:400px">
		<center>
			<h3 style="font-family: Lucida Calligraphy;">
				<font color="blue"><i><u>Login Here</u></i>
			</h3>
		</center>
		<div class="row" style="color: green">
			<div class="col-sm-2"></div>
			<div class="col-sm-8">
				<form class="form-horizontal" name="login"
					action="login" id="signup" method="post">
					${error}
					<div class="form-group">
						<label for="emailid" class="col-sm-2 control-label">Email</label>
						<div class="col-sm-10">
							<input type="email" class="form-control" id="emailid"
								name="email" required="required" placeholder="example@test.com">
							<c:if test="${not empty emailAlreadyExistError}">
								<span><font color="red">${emailAlreadyExistError}</font></span>
							</c:if>
						</div>
					</div>

					<div class="form-group">
						<label for="password" class="col-sm-2 control-label">Password</label>
						<div class="col-sm-10">
							<input type="password" class="form-control" id="password"
								name="password" required="required"
								placeholder="Minimum 8 character">
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-10">
							<center><button type="submit" class="btn btn-primary">Login</button></center>
							
						</div>
					</div>
</form>
</div></div></div>
					<%@include file="template/footer.jsp"%>

<script src="resources/js/prism.js"></script>
	<script src="resources/js/jquery-1.12.2.js"></script>
	<script src="resources/js/intlTelInput.js"></script>
	<script src="resources/js/jquery.validate.js"></script>
	<script src="resources/js/additional-methods.js"></script>
	<!-- GOOGLE ANALYTICS -->
	<script type="text/javascript" async="" src="resources/js/ga.js"></script>
	<script>
	$("#login").validate({
		  rules: {
		    "email": {
		      required: true,
		      email: true
		    },
		        "password": {
		            required: true,
		            minlength: 8,
		        },
		  }});
		  
		    </script>

</body>
</html>