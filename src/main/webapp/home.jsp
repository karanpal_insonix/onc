<%@ page language="java" contentType="text/html; charset=UTF-8"
   pageEncoding="UTF-8"%>
<%-- <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
   --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
      <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/bootstrap.css">
      <title>Home Page</title>
      <style type="text/css">
         table.two {
         border-collapse:separate;
         width:400px;
         border-spacing:10px 50px;
         }
         table.three {
         border-collapse:separate;
         width:400px;
         border-spacing:10px 50px;}
         }
         a.navbar-brand{
         font-family: sans-serif;
         color: #ff6666;
         }
         #one{
         line-height:30px;
         background-color:#b3ffff;
         height:350px;
         width:1350px;
         float:left;
         padding:5px;
         
         }
         #two{
         line-height:50px;
         background-color:lightblue;
         height:70px;
         width:1350px;
         float:left;
         padding:5px;
         opacity: .8;
         }
         #nav {
         line-height:30px;
         background-color:rgba(255, 255, 255,0.4);
         height:100px;
         width:1350px;
         float:left;
         padding:5px;
        }
        .clr{
     color:"blue";
    font-size:14px;
}
        
     .search_li{
     list-style-type: none;
    margin-top: 20px;
     }   
      </style>
   </head>
   <body background = "${pageContext.request.contextPath}/resources/img/bckground.jpg" >
      <%@include file="template/header.jsp" %>
       <div>  <center>
        
            <h3 style="font-family:Arial Rounded MT Bold;"><font color="#ff6666"><b>Find & Appointment with Doctors</b></h3></center><br><br><br><br><br><br>
    <div class="col-sm-2" STYLE="position:absolute; TOP:150px; LEFT:450px">
               <li role="separator" class="divider search_li"></li>
               <form action="DoctorSearch" name="DoctorSearch" method="get">
              
               <select class="form-control" name="location">
                  <option value="0">Location</option> 
       <c:forEach var="location" items="${locationList }">
         <option value=${location.id  }>${location.city }</option>
          </c:forEach>
             </select> </div>
              
            <li role="separator" class="divider"></li>
           
               <div class="col-sm-2" STYLE="position:absolute; TOP:150px; LEFT:670px">
               <li role="separator" class="divider search_li"></li>
               <select class="form-control" name="speciality" id="select">
                  <option value="0"> <span class="clr">Speciality </span></option>
                  
           <c:forEach var="speciality" items="${specialityList }">
               <option value="${speciality.id }"><span class="clr">${speciality.name }</span></option>
               </c:forEach> 
              </select></div>
              
               
               <div>
           <button type="submit" class="btn btn-primary navbar-btn" STYLE="position:absolute; TOP:210px; LEFT:640px"><font color="white"><b>SUBMIT</b></font></button></div>
               </div>
    
       </div>
   </form>
      
      <center><div>
         <div clas="specialities mobile-none" id="nav">
            <h2 style="Lucida Handwriting"><a href=""><font color="#006600"><i>Specialities</i></a>
            </h2>
            <h4>Combining the best specialists and equipment to provide you nothing short of the best in healthcare !!</h4>
         </div>
      </center>
      <div id="one">
      <div ><a href="dentist.jsp"><img src="${pageContext.request.contextPath}/resources/img/teeth.ico"  STYLE=" margin-top:22px; margin-left:236px; WIDTH:80px; HEIGHT:80px" ></a></div>
      <div>
      <a href="dentist.jsp">
         <h5 STYLE="Margin-top:6px; margin-left:236px"><b><i>Dentist</i><b></h5>
         <div></div></a>
         
      <a href="heart.jsp"><img src="${pageContext.request.contextPath}/resources/img/heart.ico"  STYLE="Margin-top:-115px;margin-left:480px; WIDTH:80px; HEIGHT:80px"></a></div>
      <div>
      <a href="heart.jsp">
         <h5 STYLE="Margin-top:-31px; margin-left:473px"><b><i>Cardiologist</i></b></h5>
         <div>
      <a href="ophthalmoloists.jsp"><img src="${pageContext.request.contextPath}/resources/img/eye.ico"  STYLE="Margin-top:-118px; margin-left:732px; WIDTH:80px; HEIGHT:80px"></a></div>
      <div>
      <a href="ophthalmoloists.jsp">
         <h5 STYLE="Margin-top:-30px; margin-left:719px; "><b><i>Ophthalmologists</i></b></h5>
         <div>
      <a href="neuro.jsp"><img src="${pageContext.request.contextPath}/resources/img/brain.ico" STYLE="Margin-top:-108px; margin-left:998px; WIDTH:80px; HEIGHT:80px"></a></div>
      <div>
      <a href="neuro.jsp">
         <h5 STYLE="Margin-top:-28px; margin-left:1000px"><b><i>Neurosurgeon<b></i></h5>
         <div>
      <a href="gastro.jsp"><img src="${pageContext.request.contextPath}/resources/img/Stomach-48.png"   STYLE="Margin-top:56px;margin-left:215px; WIDTH:80px; HEIGHT:80px"></a></div>
      <div>
      <a href="gastro.jsp">
         <h5 STYLE="margin-top:2px;margin-left:221px; WIDTH:80px; HEIGHT:80px"><b><i>Gastroenterologist</i></b></h5>
         <div>
      <a href="general.jsp"><img src="${pageContext.request.contextPath}/resources/img/temp.ico" STYLE="Margin-top:-182px; margin-left:485px; WIDTH:80px; HEIGHT:80px"></a></div>
      <div>
      <a href="general.jsp">
         <h5 STYLE="margin-top:-94px;margin-left:470px; WIDTH:80px; HEIGHT:80px"><b><i>General Physcian</i></b></h5>
         <div>
      <a href="ortho.jsp"><img src="${pageContext.request.contextPath}/resources/img/bone.ico"  height="80" width="80" STYLE="Margin-top:-177px; margin-left:740px; WIDTH:80px; HEIGHT:80px"></a></div>
      <div>
      <a href="ortho.jsp">
         <h5 STYLE="Margin-top:-87px; margin-left:725px; WIDTH:80px; HEIGHT:80px"><b><i>Orthopedician</i></b></h5>
         <div>
      <a href="ent.jsp"><img src="${pageContext.request.contextPath}/resources/img/plus.ico"   STYLE="Margin-top:-177px;margin-left:1005px; WIDTH:80px; HEIGHT:80px"></a></div>
      <div>
         <a href="ent.jsp">
         <h5 STYLE="Margin-top:-87px;margin-left:1028px; WIDTH:80px; HEIGHT:80px"><b><i>ENT</i></b></h5>
      </div></a></div>
      <br>
      <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jquery-1.12.2.js"></script>
      <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/bootstrap.js"></script>
      <%@include file="template/footer.jsp" %>
   </body>
</html>