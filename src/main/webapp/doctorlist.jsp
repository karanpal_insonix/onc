<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" href="resources/css/bootstrap.css">
<title>Insert title here</title>
  <style>
    table, th, td {  border: 0px solid blue; }
    th, td  { padding: 10px;  }
    th { text-align: centre; }
    table#t01 { width: 40%; background-color:rgba(0, 204, 102,0.5);}
    
    </style>
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

    <script src="js/bootstrap.min.js"></script>
</head>
<body background="resources/img/signup11.jpg"
	style="background-size: cover; background-repeat: no-repeat;">
<%@include file="template/header.jsp" %>
<%int i=0; %>
<form action="login" class="form-inline" method="post" >
<div style="height:350px">
<center><table style="width:40%">
<table id="t01">
          <tr>
            <th> Sr. No.</th>
            
            <th> First Name</th>
            <th> Last Name</th>
            <th> Speciality</th>
             <th> Location</th>
             <th> Clinic Name</th>
             <th> Pin Code</th>
            <th> License Number</th>
            <th>
            </th>
            
          </tr>
          <c:forEach var="doctor" items="${doctorList}">
           <tr>
          <td> <%=++i %></td>
          
            <td>  <font color="#008080"> ${doctor.firstName}</font></td>
            <td> <font color="#ff8533">${doctor.lastName}</font></td>
            <td> <font color="#ff8533">${doctor.speciality.name }</font></td>
            <td> <font color="#ff8533">${doctor.location.city }</font></td>
            <td> <font color="#ff8533">${doctor.location.clinicName}</font></td>
            <td> <font color="#ff8533">${doctor.location.pinNo},${doctor.location.sector} </font></td>
            <td> <font color="#ff8533">${doctor.licenseNumber }</font></td>
            <td><a href="bookAnAppointment?docId=${doctor.id}" class="btn btn-success">Book An Appointment</a></td>
          </tr>
         <!-- option value=${location.id  }>${location.city }</option-->
          </c:forEach>
         
         
          
        </table>
     </table></center></div></form>


 <%@include file="template/footer.jsp" %>

</body></html>