<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" href="resources/css/bootstrap.css">
<link rel="stylesheet" href="resources/css/datepicker.min.css" />
<link rel="stylesheet" href="resources/css/datepicker.min.css" />

<link rel="stylesheet" href="resources/css/datepicker.min.css" />
<link rel="stylesheet" href="resources/css/datepicker3.min.css" />
<link rel="stylesheet" href="resources/css/prism.css" />
<link rel="stylesheet" href="resources/css/intlTelInput.css" />

<title>Book your Appointment</title>
<style>
.iti-flag {
	background-image: url("resources/img/flags.png");
}

#error-msg {
	color: red;
}

#valid-msg {
	color: #00C900;
}

input.error {
	border: 1px solid #FF7C7C;
}

.error {
	color: red;
}

.time_slot {
	margin: 5px 20px;
}

.margin_div {
	padding: 10px 10px;
}

.aligment_text {
	text-align: center;
	font-weight: 700;
	text-decoration: underline;
}

.clr {
	background-color: rgba(0, 0, 0, 1);
}
</style>
<script src="resources/js/jquery-1.12.2.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$("input[name=appointment]").on("change", function() {
			if ($(this).val() === "yourself") {
				$("#next_form").hide();
			} else {
				$("#next_form").show();
			}
		});
	});
</script>

</head>
<body background="resources/img/appoint.jpg"
	style="background-size: cover; background-repeat: no-repeat;">
	<%@include file="template/header.jsp"%>

	<div class="container" class="clr">
		<h3 style="font-family: Lucida Calligraphy;">
			<font color="orange"><i>Book Your Appointment Here</i> </font>
		</h3>
		<div class="row" style="color: #339966">

			<div class="col-sm-6 margin_div" style="border: 2px solid orange">
				<form class="form-horizontal" name="appointment"
					action="bookAppointment" id="signup" method="post">
					<input type="hidden" name="patientId" value="${patient.id }">
					<div class="form-group">
						<label for="first_name" class="col-sm-3 control-label">FIRST
							NAME</label>
						<div class="col-sm-8">
							<input type="text" class="form-control" id="first_name"
								value="${patient.firstname}" name="firstName">
						</div>
					</div>

					<div class="form-group">
						<label for="last_name" class="col-sm-3 control-label">LAST
							NAME</label>
						<div class="col-sm-8">
							<input type="text" class="form-control" id="last_name"
								name="lastName" value="${patient.lastname }">
						</div>
					</div>



					<div class="form-group">
						<label for="appointment" class="col-sm-3 control-label">Appointment</label>
						<div class="col-sm-8">
							<div class="radio">
								<label><input type="radio" name="appointment" id="self"
									value="yourself" checked>For Yourself</label> <label><input
									type="radio" name="appointment" id="other" value="Someoneelse">For
									Someone else</label>
							</div>
						</div>
					</div>

					<!-- New form to open -->
					<div id="next_form" style="display: none">

						<!-- Add fields  -->

						<div class="form-group">
							<label for="first_name" class="col-sm-3 control-label">Patient
								First Name </label>
							<div class="col-sm-8">
								<input type="text" class="form-control" id="first_name">
							</div>
						</div>

						<div class="form-group">
							<label for="last_name" class="col-sm-3 control-label">Patient
								Last Name</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" id="last_name">
							</div>
						</div>



						<div class="form-group">
							<label for="gender" class="col-sm-3 control-label">Gender</label>
							<div class="col-sm-8">
								<div class="radio">
									<label><input type="radio" name="gender" id="gender"
										value="male">Male</label> <label><input type="radio"
										name="gender" id="gender" value="female">Female</label>
								</div>
							</div>
						</div>

						<div class="form-group">
							<label class="col-xs-3 control-label">Date</label>
							<div class="col-xs-5 date">
								<div class="input-group input-append date" id="date">
									<input type="text" class="form-control" name="date" /> <span
										class="input-group-addon add-on"><span
										class="glyphicon glyphicon-calendar"></span></span>
								</div>
							</div>
						</div>


						<!-- <div class="form-group">
							<label class="col-xs-3 control-label">DOB</label>
							<div class="col-sm-5 date">
								<div class="input-group input-append date" id="dateRangePicker">
									<input type="text" id="dob" class="form-control" name="date" />
									<span class="input-group-addon add-on"><span
										class="glyphicon glyphicon-calendar"></span></span>
								</div>
							</div>
						</div> -->

						<div class="form-group">
							<label for="mobile_no" class="col-sm-3 control-label">MobileNo</label>
							<div class="col-sm-8">
								<input id="phone" class="form-control" type="tel"> <span
									id="valid-msg" class="hide">? Valid</span> <span id="error-msg"
									class="hide">Invalid number</span>
							</div>
						</div>

						<div class="form-group">
							<label for="last_name" class="col-sm-3 control-label">Illness</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" id="illness">
							</div>
						</div>



						<!-- End of Add fields  -->
					</div>
					<!-- End of new form open -->
			</div>



			<link rel="stylesheet"
				href="https://formden.com/static/cdn/bootstrap-iso.css" />

			<!--Font Awesome (added because you use icons in your prepend/append)-->
			<link rel="stylesheet"
				href="https://formden.com/static/cdn/font-awesome/4.4.0/css/font-awesome.min.css" />

			<!-- Inline CSS based on choices in "Settings" tab -->
			<style>
.bootstrap-iso .formden_header h2, .bootstrap-iso .formden_header p,
	.bootstrap-iso form {
	font-family: Arial, Helvetica, sans-serif;
	color: black
}

.bootstrap-iso form button, .bootstrap-iso form button:hover {
	color: white !important;
}

.asteriskField {
	color: red;
}
</style>

			<!-- HTML Form (wrapped in a .bootstrap-iso div) -->
			<div class="bootstrap-iso">
				<div class="container-fluid">
					<div class="row">
						<div class="col-md-6 col-sm-6 col-xs-12">

							<div class="form-group">
							<label class="col-xs-3 control-label">Date</label>
							<div class="col-xs-5 date">
								<div class="input-group input-append date" id="date">
									<input type="text" class="form-control" name="date" /> <span
										class="input-group-addon add-on"><span
										class="glyphicon glyphicon-calendar"></span></span>
								</div>
							</div>
						</div>


							<!-- <div class="input-group input-append date" id="dateRangePicker">
								<label class="control-label " for="date"> Date </label>
								<div class="input-group">
									<div class="input-group-addon">
										<i class="fa calender"> </i>
									</div>
									<input class="form-control" id="date" name="date"
										placeholder="MM/DD/YYYY" type="text" />

								</div>
							</div> -->



							<div class="form-group ">
								<label class="control-label " for="select"> Time </label> <select
									class="select form-control" id="select" name="time">
									<c:forEach var="schedule" items="${scheduleList }">
										<option value="${schedule.id}">${schedule.time}</option>
									</c:forEach>
								</select>
								<!--  select
									class="select form-control" id="select" name="select">
									<option value="0"></option>
									<option value="0">10:00-10:15 AM</option>
									<option value="0">10:15-10:30 AM</option>
									<option value="0">10:30-10:45 AM</option>
									<option value="0">10:45-11:00 AM</option>
									<option value="0">11:00-11:15 AM</option>
									<option value="0">11:15-11:30 AM</option>
									<option value="0">11:30-11:45 AM</option>
									<option value="0">11:45-12:00 PM</option>
									<option value="0">12:00-12:15 PM</option>
									<option value="0">12:00-12:15 PM</option>
									<option value="0">12:15-12:30 PM</option>
									<option value="0">12:30-12:45 PM</option>
									<option value="0">12:45-1:00 PM</option>
									<option value="0">-------------</option>
									<option value="0">3:00-3:15 PM</option>
									<option value="0">3:15-3:30 PM</option>
									<option value="0">3:30-3:45 PM</option>
									<option value="0">3:45-4:00 PM</option>
									<option value="0">4:00-4:15 PM</option>
									<option value="0">4:15-4:30 PM</option>
									<option value="0">4:30-4:45 PM</option>
									<option value="0">4:45-5:00 PM</option>
									<option value="0">5:00-5:15 PM</option>
									<option value="0">5:15-5:30 PM</option>
									<option value="0">5:30-5:45 PM</option>
									<option value="0">5:45-6:00 PM</option>
									<option value="0">6:00-6:15 PM</option>
									<option value="0">6:15-6:30 PM</option>
									<option value="0">6:30-6:45 PM</option>
									<option value="0">6:45-7:00 PM</option>
								</select-->
							</div>
							<div class="form-group">
								<div>
									<button class="btn btn-primary " name="submit" type="submit">
										Submit</button>
								</div>
							</div>
							</form>
						</div>
					</div>
				</div>
			</div>
			</form>
		</div>

	</div>









	</div>



	<%@include file="template/footer.jsp"%>
	<!-- start of jquery -->
	<script src="resources/js/prism.js"></script>
	<script src="resources/js/jquery-1.12.2.js"></script>
	<script src="resources/js/intlTelInput.js"></script>
	<script src="resources/js/jquery.validate.js"></script>
	<script src="resources/js/additional-methods.js"></script>
	<!-- GOOGLE ANALYTICS -->
	<script type="text/javascript" async="" src="resources/js/ga.js"></script>

	<!-- DatePicker JS -->
	<script>
		$(document).ready(function() {
			$('#date').datepicker({
				format : 'mm/dd/yyyy'
			}).on('changeDate', function(e) {
				// Revalidate the date field
				//$('#eventForm').formValidation('revalidateField', 'date');
			});

			$('#eventForm').formValidation({
				framework : 'bootstrap',
				icon : {
					valid : 'glyphicon glyphicon-ok',
					invalid : 'glyphicon glyphicon-remove',
					validating : 'glyphicon glyphicon-refresh'
				},
				fields : {
					name : {
						validators : {
							notEmpty : {
								message : 'The name is required'
							}
						}
					},
					date : {
						validators : {
							notEmpty : {
								message : 'The date is required'
							},
							date : {
								format : 'MM/DD/YYYY',
								message : 'The date is not a valid'
							}
						}
					}
				}
			});
		});
	</script>


	<!-- <script>
		var _gaq = _gaq || [];
		_gaq.push([ '_setAccount', 'UA-26178728-1' ]);
		_gaq.push([ '_trackPageview' ]);
		(function() {
			var ga = document.createElement('script');
			ga.type = 'text/javascript';
			ga.async = true;
			ga.src = ('https:' == document.location.protocol ? 'https://ssl'
					: 'http://www')
					+ '.google-analytics.com/ga.js';
			var s = document.getElementsByTagName('script')[0];
			s.parentNode.insertBefore(ga, s);
		})();
	</script>
	/GOOGLE ANALYTICS -->
	<script src="resources/js/bootstrap-datepicker.min.js"></script>
	<script>
$(function(){
	$("#dob").datepicker({
            format: 'mm-dd-yyyy'
        })
        .on('changeDate', function(e) {
            console.log("Revalidate the date field");
            
        });
 
	var telInput = $("#phone"),
  errorMsg = $("#error-msg"),
  validMsg = $("#valid-msg");

// initialise plugin
telInput.intlTelInput();

var reset = function() {
  telInput.removeClass("error");
  errorMsg.addClass("hide");
  validMsg.addClass("hide");
};

// on blur: validate
telInput.blur(function() {
  reset();
  if ($.trim(telInput.val())) {
    if (telInput.intlTelInput("isValidNumber")) {
      validMsg.removeClass("hide");
    } else {
      telInput.addClass("error");
      errorMsg.removeClass("hide");
    }
  }
});

// on keyup / change flag: reset
//telInput.on("keyup change", reset);

/*--------------------      form validation  ------------------------*/

<script src="resources/js/bootstrap-datepicker.min.js"></script>
	<script>
$(function(){
	$("#date").datepicker({
            format: 'mm-dd-yyyy'
        })
        .on('changeDate', function(e) {
            console.log("Revalidate the date field");
            
        });
 
	
/*--------------------      form validation  ------------------------*/
$("#signup").validate({
		  rules: {
		    "email": {
		      required: true,
		      email: true
		    },
		        "password": {
		            required: true,
		            minlength: 8
		        },
		        "password_confirm":{
		            required: true,
		            minlength: 8,
		            equalTo: "#password"
		        },
		        "firstname":{
		        	required: true,
		        	 myField: { lettersonly: true }
		        	},
		        	"lastname":{
		        		required:true,
		        		 myField: { lettersonly: true }
		        	},
		        "date":{
		        	required:true
		        },
		        "gender":{
		        	required:true
		        	},	
		        "phone":{
		        	required:true
		       }
		        },
 				submitHandler: function(form) {
                            // do other things for a valid form
                            form.submit();
				}
	});


/*-------------------- end of form validation  ------------------------*/

/*-------------button disable code--------------------------------*/
		
$('input:submit').click(function(){
	$('p').text("Form submiting.....").addClass('submit');
	$('input:submit').attr("disabled", true);	
});

$('button').click(function(){
	$('p').text("I'm a form ~").removeClass('submit');
	$('input:submit').attr("disabled", false);	
});



/*-------------button disable code--------------------------------*/


});
</script>
	<!--end of start of jquery -->
</body>
</html>