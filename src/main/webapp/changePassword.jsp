<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Change Password</title>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" href="resources/css/bootstrap.css">
<style>
.iti-flag {background-image: url("resources/img/flags.png");}
#error-msg {
  color: red;
}
#valid-msg {
  color: #00C900;
}
input.error {
  border: 1px solid #FF7C7C;
}
.error{
color:red;
}
</style>
  <script src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.12.0.min.js"></script>
<script src="resources/js/jquery.validate.js"></script>
<script src="resources/js/additional-methods.js"></script>

<script>
$(document).ready(function(){

	$( "#changepassword" ).validate({
		  rules: {

		        "New-password": {
		            required: true,
		            minlength: 8
		        },
		        "Confirm-password": {
		            required: true,
		            minlength: 8,
		            equalTo: "#New-password"
		        }
		        }
	}
	);
	});


</script>
<style>
label {

    max-width: 100%;
    margin-bottom: 2px;
    font-weight: bold;
        margin-top: 2px;
}


</style>
</head>
<body background="resources/img/signup11.jpg" style="background-size:cover; background-repeat:no-repeat;">
<div>
<center><h1 style="font-family:Franklin Gothic Heavy;"><font color="#009933"><i>Change Password</i></h1></center>

<center>
<form action="changePass" id="changepassword" method="POST">
  <input type="hidden" name="id" value="${user.id }">

<div class="form-group">
    <label for="New-password" class="col-sm-6 control-label">New Password</label>
    <div class="col-sm-4">
      <input type="password" class="form-control" id="New-password" name="password" required="required" placeholder="Minimum 8 character" >
  </div>
  </div>

  <div class="form-group" float="none">
    <label for="Confirm-password" class="col-sm-6 control-label">Confirm Password</label>
    <div class="col-sm-4">
      <input type="Password" class="form-control" id="Confirm-password" name="Confirm-password"  required="required" placeholder="Re-Type password" >
  </div>
  </div>
  <div class="form-group">
  <div class="col-sm-offset-2 col-sm-10">

 <button type="submit" class="btn btn-primary">Submit</button>
 </div>
</div>
</form>
</center>
</div>
</body>
</html>
