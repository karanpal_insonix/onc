package com.doconcall.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DBUtil 
{
	private final String url = "jdbc:mysql://localhost/doconcall";
	private final String username = "root";
	private final String password = "12345678";
	private final String driverClassName = "com.mysql.jdbc.Driver";
	private Connection con;
		
	public Connection getConnection(){
		try{
			Class.forName(driverClassName); 
			con = DriverManager.getConnection(url,username,password);
		}
		catch(ClassNotFoundException e){
			e.printStackTrace();
		}
		catch(SQLException e){
			e.printStackTrace();
		}
		return con;
	} 
	
	public void close(Connection con){
		if(con != null){
			try {
				con.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public void close(Statement stat){
		if(stat != null){
			try {
				stat.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public void close(ResultSet result){
		if(result != null){
			try {
				result.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public void commit(Connection con) {
		if(con != null)
			try {
				con.commit();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		
	}
	
	public void rollback(Connection con) {
		if(con != null)
			try {
				con.rollback();
			} catch (SQLException e) {
				e.printStackTrace();
			}
	}
	
}

	
	
	
	
	
	


