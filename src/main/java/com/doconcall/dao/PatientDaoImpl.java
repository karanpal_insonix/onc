package com.doconcall.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.doconcall.model.Location;
import com.doconcall.model.Patient;

public class PatientDaoImpl implements PatientDao {
	private DBUtil dbutil = new DBUtil();
	private Connection conn = null;
	private PreparedStatement stat = null;
	private ResultSet result = null;
	private List<Patient> patientList;
	Patient patient = null;

	public Patient create(Patient patient) {

		try {
			String query = "insert into patient (first_name, last_name,dob,gender,user_id) values(?,?,?,?,?)";
			conn = dbutil.getConnection();
			PreparedStatement stat = conn.prepareStatement(query);

			stat.setString(1, patient.getFirstname());
			stat.setString(2, patient.getLastname());
			stat.setString(3, patient.getDob());
			stat.setString(4, patient.getGender());
			stat.setLong(5, patient.getUserId());
			stat.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			dbutil.close(stat);
			// dbutil.close(result);
			dbutil.close(conn);
		}
		return patient;
	}

	@Override
	public Patient update(Patient patient) {
		conn = dbutil.getConnection();
		try {
			String query = "update patient set firstName=?, lastName=?, licenceNumber=?, specialityId=?,locationId=?,specialityIdAndlocationId=? where userId=?";
			PreparedStatement preparedStatement = conn.prepareStatement(query);
			preparedStatement.setLong(1, result.getLong("id"));
			preparedStatement.setString(2, result.getString("first_name"));
			preparedStatement.setString(3, result.getString("last_name"));
			preparedStatement.setString(4, result.getString("dob"));
			preparedStatement.setString(5, result.getString("gender"));
			preparedStatement.executeUpdate();
			preparedStatement.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		finally {
			dbutil.close(stat);
			// dbutil.close(result);
			dbutil.close(conn);
		}

		return patient;

	}

	@Override

	public Patient getById(Long id) {
		System.out.println(id);

		try {

			String query = "select * from patient where user_id=?";
			conn = dbutil.getConnection();
			PreparedStatement stat = conn.prepareStatement(query);
			stat.setLong(1, id);
			result = stat.executeQuery();
			if (result.next()) {
				patient = new Patient();
				patient.setDob("dob");
				patient.setFirstname(result.getString("first_name"));
				patient.setLastname(result.getString("last_name"));
				patient.setGender(result.getString("gender"));
				patient.setId(result.getLong("id"));
			}

		} catch (SQLException e) {
			e.printStackTrace();

		}
		finally {
			dbutil.close(stat);
			dbutil.close(result);
			dbutil.close(conn);
		}
		return patient;
	}

	@Override
	public Patient delete(Long id) {

		return null;
	}

	@Override
	public List<Patient> getAll() {
		conn = dbutil.getConnection();
		List<Patient> patientList = new ArrayList<>();
		try {
			conn.setAutoCommit(false);

			stat = conn.prepareStatement("select * from patient");
			result = stat.executeQuery();
			while (result.next()) {
				Patient patient = new Patient();
				patient.setId(result.getLong("id"));
				patient.setFirstname(result.getString("first_name"));
				patient.setLastname(result.getString("last_name"));
				patient.setDob(result.getString("dob"));
				patient.setGender(result.getString("gender"));

				patientList.add(patient);
			}
			conn.commit();
		} catch (SQLException e) {
			e.printStackTrace();
			try {
				conn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}

		finally {
			dbutil.close(stat);
			dbutil.close(result);
			dbutil.close(conn);
		}

		return patientList;
	}

	@Override
	public Patient get(Long id) {
		try {

			String query = "select * from patient where id=?";
			conn = dbutil.getConnection();
			PreparedStatement stat = conn.prepareStatement(query);
			stat.setLong(1, id);
			result = stat.executeQuery();
			if (result.next()) {
				patient = new Patient();
				patient.setDob("dob");
				patient.setFirstname(result.getString("first_name"));
				patient.setLastname(result.getString("last_name"));
				patient.setGender(result.getString("gender"));
				patient.setId(result.getLong("id"));
			}

		} catch (SQLException e) {
			e.printStackTrace();

		}
		finally {
			dbutil.close(stat);
			dbutil.close(result);
			dbutil.close(conn);
		}
		return patient;

	}

	public static void main(String[] args) {
		Patient p = new Patient();
		p.setFirstname("aaa");
		p.setLastname("fff");
		p.setGender("m");
		p.setDob("23-02-02");
		p.setUserId(5L);
		PatientDao self = new PatientDaoImpl();
		self.create(p);
	}

}