package com.doconcall.dao;

import java.util.List;

import com.doconcall.model.Location;

public interface LocationDao {
	public Location create(Location loc);
	public Location update(Location loc);
	public Location getById(Long id);
	public boolean delete(Long id);
	public List<Location> getAll();
}
