package com.doconcall.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.doconcall.model.Speciality;

public class SpecialityDaoImpl implements SpecialityDao
{
	private DBUtil dbUtil = new DBUtil();
	private Connection con = null;
	private PreparedStatement stat = null;
	private ResultSet result = null;
	
	
	
	@Override
	public Speciality create(Speciality spe) {
		// TODO Auto-generated method stub
		return null;
	}

	
		public List<Speciality> getAll()
		{
			con = dbUtil.getConnection();
			List<Speciality> specialityList = new ArrayList<>();
			try {
				con.setAutoCommit(false);

				stat = con.prepareStatement("select * from speciality");
				result = stat.executeQuery();
				while(result.next()) {
					Speciality spe = new Speciality();
					spe.setId(result.getLong("id"));
					spe.setName(result.getString("name"));
					 specialityList.add(spe);
				}
				con.commit();
			} catch (SQLException e) {
				e.printStackTrace();
				try {
					con.rollback();
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
			}
			
			finally {
				dbUtil.close(stat);
				dbUtil.close(result);
				dbUtil.close(con);
			}
			return specialityList;
		}
	
		public static void main(String[] args) {
			SpecialityDao specialityDao = new SpecialityDaoImpl();
			System.out.println(specialityDao.getAll());
		}
	
	
	}


