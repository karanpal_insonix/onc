package com.doconcall.dao;

import java.util.List;

import com.doconcall.model.Doctor;
import com.doconcall.model.Schedule;

public interface DoctorDao {
	public Doctor create(Doctor doc);
	public Doctor update(Doctor doc);
	public Doctor getById(Long id);
	public List<Doctor> getByLocation(Long locationId);
	public List<Doctor> getBySpeciality(Long specialityId);
	public List<Doctor> getSpecialityIdAndlocationId(Long specialityId,Long locationId);
	public List<Doctor> getAll();
	List<Doctor> getSpecialityIdAndlocationId();
	public List<Schedule> getByScheduleId(Long id);
	public Schedule getScheduleById(Long id);
	
	public Schedule blockSchedule(Long id);

	
}	
