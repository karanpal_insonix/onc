package com.doconcall.dao;

import java.util.List;

import com.doconcall.model.User;

public interface UserDao 
{
	public User register(User user)throws UserAlreadyExistsException;
	public List<User> getAll();
	public User getUser(Long id);
	public User update(User user);
	public User getUserByEmail(String email);
	public User authenticate(String email, String password);


}
