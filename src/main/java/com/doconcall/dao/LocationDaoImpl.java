package com.doconcall.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.doconcall.model.Location;

public class LocationDaoImpl implements LocationDao {
	private DBUtil dbUtil = new DBUtil();
	private Connection con = null;
	private PreparedStatement stat = null;
	private ResultSet result = null;
	
	
	@Override
	public Location create(Location loc) {
		return null;
	}

	@Override
	public Location update(Location loc) {
		return null;
	}

	@Override
	public Location getById(Long id) {
		return null;
	}

	@Override
	public boolean delete(Long id) {
		return false;
	}

	@Override
	public List<Location> getAll() {
		con = dbUtil.getConnection();
		List<Location> locationList = new ArrayList<>();
		try {
			con.setAutoCommit(false);

			stat = con.prepareStatement("select distinct (l.city),id from location l group by l.city;");
			result = stat.executeQuery();
			while(result.next()) {
				Location loc = new Location();
				loc.setId(result.getLong("id"));
				//loc.setSector(result.getString("sector"));
				loc.setCity(result.getString("city"));
				//loc.setClinicName(result.getString("clinic_name"));
				//loc.setPinNo((long)(result.getInt("pin_no")));
				//loc.setDoctorId(result.getLong("doctor_id"));
				locationList.add(loc);
			}
			con.commit();
		} catch (SQLException e) {
			e.printStackTrace();
			try {
				con.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}
		
		finally {
			dbUtil.close(stat);
			dbUtil.close(result);
			dbUtil.close(con);
		}
		return locationList;
	}
	
	public static void main(String[] args) {
		LocationDao locationDao = new LocationDaoImpl();
		System.out.println(locationDao.getAll());
	}

}
