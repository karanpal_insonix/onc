package com.doconcall.dao;

import java.util.List;

import com.doconcall.model.Location;
import com.doconcall.model.Speciality;

public interface SpecialityDao 
{
	public Speciality create(Speciality spe);
	
	public List<Speciality> getAll();

}
