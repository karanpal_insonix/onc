package com.doconcall.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.print.Doc;

import com.doconcall.model.Appointment;
import com.doconcall.model.Doctor;
import com.doconcall.model.Patient;
import com.doconcall.model.Schedule;

public class AppointmentDaoImpl {
	private DBUtil dbUtil = new DBUtil();
	private Connection con = null;
	private PreparedStatement stat = null;
	private ResultSet result = null;
	
	public Appointment saveAppointment(Appointment appointment){
		try {
			String query = "insert into appointment (patient_id,doctor_id,schedule_id,reason) values(?,?,?,?)";
			con = dbUtil.getConnection();			
			PreparedStatement stat = con.prepareStatement(query,Statement.RETURN_GENERATED_KEYS);
			stat.setLong(1, appointment.getPatient().getId());
			stat.setLong(2, appointment.getDoctor().getId());
			stat.setLong(3, appointment.getSchedule().getId());
			stat.setString(4, appointment.getReason());			
			stat.executeUpdate();
			result = stat.getGeneratedKeys();
	         if(result.next()){
	        	 appointment.setId(result.getLong(1));
	         }

		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			dbUtil.close(stat);
			dbUtil.close(result);
			dbUtil.close(con);
		}
		return appointment;
	}
	
	
	
	public static void main(String[] args) {
		AppointmentDaoImpl self = new AppointmentDaoImpl();
		Appointment appointment = new Appointment();
		Doctor doctor = new Doctor();
		doctor.setId(1l);
		Patient patient = new Patient();
		patient.setId(1L);
		Schedule schedule = new Schedule();
		schedule.setId(1l);
		appointment.setDoctor(doctor);
		appointment.setPatient(patient);
		appointment.setSchedule(schedule);
		appointment.setReason("Fever");
		
		appointment = self.saveAppointment(appointment);
		
		System.out.println(appointment);
	}
}
