package com.doconcall.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.doconcall.model.Doctor;
import com.doconcall.model.Location;
import com.doconcall.model.Schedule;
import com.doconcall.model.Speciality;

public class DoctorDaoImpl implements DoctorDao {
	private DBUtil dbUtil = new DBUtil();
	private Connection con = null;
	private PreparedStatement stat = null;
	private ResultSet result = null;
	String sqlSelectRecord = null;

	@Override
	public Doctor create(Doctor doctor) {
		try {
			String query = "insert into doctor (id,first_name,last_name,license_number,user_id,speciality_id,location_id) values(?,?,?,?,?,?,?)";
			con = dbUtil.getConnection();

			PreparedStatement stat = con.prepareStatement(query);
			stat.setLong(1, doctor.getId());

			stat.setString(2, doctor.getFirstName());
			stat.setString(3, doctor.getLastName());
			stat.setString(4, doctor.getLicenseNumber());
			stat.setLong(5, doctor.getUserId());

			stat.setLong(6, doctor.getSpecialityId());
			stat.setLong(7, doctor.getLocationId());
			System.out.println("1232154r325687");
			stat.executeUpdate();
			System.out.println("1kjadfhs135y742t8");
		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			dbUtil.close(stat);
			// dbutil.close(result);
			dbUtil.close(con);
		}

		return doctor;
	}

	@Override
	public Doctor update(Doctor doctor) {

		try {
			String query = "update patient set id=?, first_name=?, last_name=?, licence_number=?, speciality_id=?,location_id=? where userId=?";

			con = dbUtil.getConnection();
			PreparedStatement stat = con.prepareStatement(query);
			stat.setLong(1, doctor.getId());

			stat.setString(2, doctor.getFirstName());
			stat.setString(3, doctor.getLastName());
			stat.setString(4, doctor.getLicenseNumber());
			stat.setLong(5, doctor.getUserId());

			stat.setLong(6, doctor.getSpecialityId());
			stat.setLong(7, doctor.getLocationId());

			stat.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			dbUtil.close(stat);
			// dbutil.close(result);
			dbUtil.close(con);
		}

		return doctor;
	}

	@Override
	public Doctor getById(Long id) {
		Doctor doc = new Doctor();	
		con = dbUtil.getConnection();
		try {
			String query = "select d.id as doc_id,d.first_name as doc_first_name,"
					+ "d.last_name as doc_last_name, d.license_number as doc_license_number,"
					+ "d.user_id as doc_user_id, l.clinic_name as loc_clinic_name, l.pin_no as loc_pin_no,"
					+ "l.city as loc_city, l.sector as loc_sector from doctor d inner join location l on d.location_id = l.id where d.id = ?";
					
			stat = con.prepareStatement(query);
			stat.setLong(1, id);
			result = stat.executeQuery();

			while (result.next()) {				
				doc.setId(result.getLong("doc_id"));
				doc.setFirstName(result.getString("doc_first_name"));
				doc.setLastName(result.getString("doc_last_name"));
				doc.setLicenseNumber(result.getString("doc_license_number"));
				doc.setUserId(result.getLong("doc_user_id"));
				doc.setClinicName(result.getString("loc_clinic_name"));
				doc.setPinNo((long)result.getInt("loc_pin_no"));
				doc.setCity(result.getString("loc_city"));
				doc.setSector((result.getString("loc_sector")));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			dbUtil.close(stat);
			dbUtil.close(result);
			dbUtil.close(con);
		}
		return doc;
	}

	@Override
	public List<Doctor> getByLocation(Long locationId) {

		/*
		 * try{ String query="select* from doctor where location_id=?";
		 * PreparedStatement stat = con.prepareStatement( "query" );
		 * 
		 * 
		 * } catch(SQLException e){ e.printStackTrace(); }
		 */

		return null;
	}

	@Override
	public List<Doctor> getSpecialityIdAndlocationId(Long specId, Long locId) {
		con = dbUtil.getConnection();
		List<Doctor> doctors = new ArrayList<>();
		try {

			String query = "select s.id as spec_id, s.name as spec_name,d.id as doc_id, d.first_name as doc_first_name, d.license_number as lic_number, "
					+ "d.user_id as doc_user_id, d.last_name as doc_last_name, l.id as loc_id, "
					+ "l.sector as loc_sector, l.city as city ,l.clinic_name as clinic_name, l.pin_no as pin_no ,l.doctor_id as doctor_id from speciality s inner join doctor d on d.speciality_i"
					+ "d = s.id inner join " + "location l on d.location_id = l.id where l.id = ? and s.id = ?";

			/*
			 * if (locId == null){
			 * 
			 * String query1 =
			 * "select s.id as spec_id, s.name as spec_name,d.id as doc_id, d.first_name as doc_first_name, d.license_number as lic_number, "
			 * + "d.user_id as doc_user_id, d.last_name as doc_last_name,  " +
			 * "speciality s inner join doctor d on d.speciality_i" +
			 * "d = s.id inner join " + " s.id = ?";
			 */

			/*
			 * String query2 =
			 * "select s.id as spec_id, s.name as spec_name,d.id as doc_id, d.first_name as doc_first_name, d.license_number as lic_number, "
			 * + "d.user_id as doc_user_id, d.last_name as doc_last_name,"
			 * //l.id as loc_id, " //+
			 * "l.sector as loc_sector, l.city as city from speciality s inner join doctor d on d.speciality_i"
			 * + "d = s.id inner join " +
			 * "speciality l on d.speciality_id = l.id  s.id = ?";
			 */

			stat = con.prepareStatement(query);
			// stat = con.prepareStatement(query1);
			stat.setLong(1, locId);
			stat.setLong(2, specId);
			result = stat.executeQuery();

			while (result.next()) {
				Doctor doc = new Doctor();
				Speciality speciality = new Speciality();
				Location location = new Location();

				speciality.setId(result.getLong("spec_id"));
				speciality.setName(result.getString("spec_name"));
				doc.setId(result.getLong("doc_id"));
				doc.setFirstName(result.getString("doc_first_name"));
				doc.setLastName(result.getString("doc_last_name"));
				doc.setLicenseNumber(result.getString("lic_number"));
				doc.setUserId(result.getLong("doc_user_id"));

				location.setId(result.getLong("loc_id"));
				location.setSector(result.getString("loc_sector"));
				location.setCity(result.getString("city"));
				location.setClinicName(result.getString("clinic_name"));
				
				location.setPinNo((long)result.getInt("pin_no"));
				doc.setLocation(location);
				doc.setSpeciality(speciality);

				doctors.add(doc);

			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			dbUtil.close(stat);
			dbUtil.close(result);
			dbUtil.close(con);
		}
		return doctors;
	}

	@Override
	public List<Doctor> getAll() {

		con = dbUtil.getConnection();
		List<Doctor> doctorList = new ArrayList<>();
		try {
			con.setAutoCommit(false);

			stat = con.prepareStatement("select * from doctor");
			result = stat.executeQuery();
			while (result.next()) {
				Doctor doc = new Doctor();
				doc.setId(result.getLong("id"));
				doc.setSpecialityId(result.getLong("speciality_id"));
				/*
				 * doc.setSpecialityIdAndlocationId(result.getString(
				 * "specialityIdAndlocationId"));
				 */
				doc.setLocationId(result.getLong("location_id"));
				doc.setFirstName(result.getString("first_name"));
				doc.setLastName(result.getString("last_name"));
				doc.setLicenseNumber("" + result.getLong("license_number"));

				doctorList.add(doc);
			}
			con.commit();
		} catch (SQLException e) {
			e.printStackTrace();
			try {
				con.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}

		finally {
			dbUtil.close(stat);
			dbUtil.close(result);
			dbUtil.close(con);
		}
		System.out.println(doctorList);
		return doctorList;
	}

	@Override
	public List<Doctor> getBySpeciality(Long specialityId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Doctor> getSpecialityIdAndlocationId() {
		// TODO Auto-generated method stub
		return null;
	}

	public static void main(String[] args) {
		DoctorDao doctorDao = new DoctorDaoImpl();
		System.out.println(doctorDao.getById(9l));
	}

	@Override
	public List<Schedule> getByScheduleId(Long id) {
		// TODO Auto-generated method stub

		con = dbUtil.getConnection();
		List<Schedule> doctorScheduleList = new ArrayList<>();
		try {
			con.setAutoCommit(false);

			stat = con.prepareStatement("select * from schedule where doctor_id=? and filled=0");
			stat.setLong(1, id);

			result = stat.executeQuery();
			while (result.next()) {
				Schedule doc = new Schedule();
				doc.setId(result.getLong("id"));
				/*
				 * doc.setSpecialityIdAndlocationId(result.getString(
				 * "specialityIdAndlocationId"));
				 */
				// doc.setLocationId(result.getLong("location_id"));
				doc.setDate(result.getString("date"));
				doc.setTime(result.getString("time"));
				// doc.set(result.getString("date"));
				doc.setFilled(result.getString("filled"));

				// doc.set("" + result.getLong("license_number"));
				System.out.println(doc);
				doctorScheduleList.add(doc);
			}
			con.commit();
		} catch (SQLException e) {
			e.printStackTrace();
			try {
				con.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		} finally {
			dbUtil.close(stat);
			dbUtil.close(result);
			dbUtil.close(con);
		}
		System.out.println(doctorScheduleList);
		return doctorScheduleList;
	}
	
	@Override
	public Schedule getScheduleById(Long id){
		con = dbUtil.getConnection();
		Schedule schedule =null;
		try {
			con.setAutoCommit(false);
			stat = con.prepareStatement("select * from schedule where id = ?");
			stat.setLong(1, id);
			result = stat.executeQuery();
			while (result.next()) {
				schedule = new Schedule();
				schedule.setId(result.getLong("id"));
				schedule.setDate(result.getString("date"));
				schedule.setTime(result.getString("time"));
				schedule.setFilled(result.getString("filled"));
				
			}
			con.commit();
		} catch (SQLException e) {
			e.printStackTrace();
			try {
				con.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		} finally {
			dbUtil.close(stat);
			dbUtil.close(result);
			dbUtil.close(con);
		}
		
		return schedule;
	}

	@Override
	public Schedule blockSchedule(Long id) {
		
		Schedule schedule = getScheduleById(id);
		con = dbUtil.getConnection();
		try {
			con.setAutoCommit(false);
			stat = con.prepareStatement("update schedule set filled = true where id = ?");
			stat.setLong(1, id);
			int rows = stat.executeUpdate();
			if(rows > 0){
				schedule.setFilled("true");
			}
			con.commit();
		} catch (SQLException e) {
			e.printStackTrace();
			try {
				con.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		} finally {
			dbUtil.close(stat);
			dbUtil.close(result);
			dbUtil.close(con);
		}
		
		return schedule;
	}
	
	
}
