package com.doconcall.dao;

import java.util.List;

import com.doconcall.model.User;

import java.sql.*;

public class UserDaoImpl implements UserDao 
{
	private Connection con;
	private PreparedStatement stat;
	private ResultSet result;
	private DBUtil dbutil=new DBUtil();
	

	@Override
	public User register(User user)throws UserAlreadyExistsException{

		User dbUser = getUserByEmail(user.getEmail());
		if(dbUser != null)
			throw new UserAlreadyExistsException("User with this email already exists");
		con=dbutil.getConnection();
		try{
		 String query = "insert into user (email,password) values (?,?)";
         PreparedStatement preparedStatement = con.prepareStatement( query,Statement.RETURN_GENERATED_KEYS );
         preparedStatement.setString( 1, user.getEmail() );
         preparedStatement.setString( 2, user.getPasword() );
        
         preparedStatement.executeUpdate();
         ResultSet result = preparedStatement.getGeneratedKeys();
         if(result.next()){
        	 user.setId(result.getLong(1));
         }
         preparedStatement.close();
     } catch (SQLException e) {
         e.printStackTrace();
     }
		finally{
			
			dbutil.close(con);
			
		}	
return user;

		
}


	@Override
	public List<User> getAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public User getUser(Long id) {
		return null;
	}

	@Override
	public User update(User user) {
		con = dbutil.getConnection();
		String query = "UPDATE user SET password =? WHERE id= ?";
		try{
			stat = con.prepareStatement(query);
			stat.setString(1, user.getPasword());
			stat.setLong(2, user.getId());
			int rows = stat.executeUpdate();
			if(rows > 0){
				return user;
			}
			
		}
		catch(SQLException e){
			e.printStackTrace();
		}
		finally{
			dbutil.close(result);		
			dbutil.close(result);
			dbutil.close(stat);
			dbutil.close(con);
			
		}		
		
		return null;
		
	}

	@Override
	public User authenticate(String email, String password) {
		User user=null;
		con=dbutil.getConnection();
		String query="select * from user where email=? and password=?";
		try{
			stat=con.prepareStatement(query);
			stat.setString(1,email);
			stat.setString(2,password);
			result=stat.executeQuery();
			if(result.next()){
				user=new User();
				user.setId(result.getLong("id"));
				user.setEmail(result.getString("email"));
				user.setPassword(result.getString("password"));
			}
		}catch(SQLException e){
			e.printStackTrace();
		}
		finally{
			dbutil.close(result);		
			dbutil.close(result);
			dbutil.close(stat);
			dbutil.close(con);
			
		}		
		return user;
	}
	
	
	@Override
	public User getUserByEmail(String email) {
		con = dbutil.getConnection();
		String query = "select * from user where email = ?";		
		User user = null;
		try {
			stat = con.prepareStatement(query);
			stat.setString(1, email);
			result = stat.executeQuery();
			if(result.next()){
				user = new User();
				user.setId(result.getLong("id"));
				user.setEmail(result.getString("email"));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return user;
	}


	public static void main(String[] args) {
		UserDao userDao = new UserDaoImpl();
		//System.out.println(userDao.authenticate("kusma.singh04@gmail.com", "12345"));
		User user=new User("abcdef@gmail.com","123456");
		try {
			userDao.register(user);
		} catch (UserAlreadyExistsException e) {
			System.out.println(e.getMessage());
		}
	}


	/*//@Override
	public User ChangePassword(User user) {
		con = dbutil.getConnection();
		String query = "update user SET password=newpassword where email = ?";
		try{
			stat=con.prepareStatement(query);
			result=stat.executeQuery();
				}
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null ;
	
	
	
	}


	Override
	public User ChangePassword() {
		// TODO Auto-generated method stub
		return null;
	}
*/

	

}
