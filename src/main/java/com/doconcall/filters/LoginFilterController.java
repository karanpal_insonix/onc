package com.doconcall.filters;

import java.io.IOException;

import javax.servlet.DispatcherType;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.doconcall.model.User;

/**
 * Servlet Filter implementation class LoginFilterController
 */
@WebFilter(dispatcherTypes = {
				DispatcherType.REQUEST, 
				DispatcherType.FORWARD, 
				DispatcherType.INCLUDE, 
				DispatcherType.ERROR
		}
					, urlPatterns = { "/bookAnAppointment" }, servletNames = { "AppointmentServlet" })
public class LoginFilterController implements Filter {

    /**
     * Default constructor. 
     */
    public LoginFilterController() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		// TODO Auto-generated method stub
		// place your code here
System.out.println("Filter");
		HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse res = (HttpServletResponse) response;
		 HttpSession session = req.getSession(false);
	       // User user = (session != null) ? (User) session.getAttribute("user") : null;
		 User user = (User) session.getAttribute("user");
	       
System.out.println(""+user);
	        if (user == null ) {
	        	request.setAttribute("error","Please login before booking an appointment");
	        	req.getRequestDispatcher("login.jsp").forward(req, res);
	           // res.sendRedirect("login.jsp");
	        } else {
	        	chain.doFilter(request, response);
			} 
		// pass the request along the filter chain
		//chain.doFilter(request, response);
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
	}

}
