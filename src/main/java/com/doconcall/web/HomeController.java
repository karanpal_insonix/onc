package com.doconcall.web;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.doconcall.model.Location;
import com.doconcall.model.Speciality;
import com.doconcall.service.LocationService;
import com.doconcall.service.LocationServiceImpl;
import com.doconcall.service.SpecialityService;
import com.doconcall.service.SpecialityServiceImpl;

@WebServlet("/home")
public class HomeController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       private LocationService locationService = new LocationServiceImpl();
       private SpecialityService specialityService = new SpecialityServiceImpl();
    public HomeController() {
        super();
        // TODO Auto-generated constructor stub
    }

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		List<Location> locationList = locationService.getAll();
		
		
		List<Speciality> specialityList = specialityService.getAll();
		
		for(Speciality spe:specialityList) {
			System.out.println(spe);
		}
		
		
		request.setAttribute("locationList", locationList);
		request.setAttribute("specialityList", specialityList);
		request.getRequestDispatcher("/home.jsp").forward(request, response);
		
		//response.getWriter().append("Served at: ").append(request.getContextPath());
			
		
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
