package com.doconcall.web;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.doconcall.model.Doctor;
import com.doconcall.model.Location;
import com.doconcall.model.Speciality;
import com.doconcall.service.DoctorService;
import com.doconcall.service.DoctorServiceImpl;
import com.doconcall.service.LocationService;
import com.doconcall.service.LocationServiceImpl;
import com.doconcall.service.SpecialityService;
import com.doconcall.service.SpecialityServiceImpl;

@WebServlet("/DoctorSearch")
public class DoctorSearchController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	DoctorService doctorService = new DoctorServiceImpl();
	SpecialityService specialityService = new SpecialityServiceImpl();
	LocationService locationService = new LocationServiceImpl();

	public DoctorSearchController() {
		super();

	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.getWriter().append("Served at: ").append(request.getContextPath());
		doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String speciality = request.getParameter("speciality");
		String location = request.getParameter("location");
				
		System.out.println(location);
		System.out.println(speciality);
		
		Long locationId = Long.parseLong(location);
		Long specialityId = Long.parseLong(speciality);
		
		if(locationId == 0){
			location = null;
		}
		
		if(specialityId == 0){
			speciality = null;
		}
		
		List<Doctor> doctorList = doctorService.getSpecialityIdAndlocationId(specialityId,locationId );
		System.out.println("speciality" + speciality + "," + location);

		request.setAttribute("doctorList", doctorList);

		RequestDispatcher rd = request.getRequestDispatcher("doctorlist.jsp");
		rd.forward(request, response);
	}

}
