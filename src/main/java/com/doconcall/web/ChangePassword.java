package com.doconcall.web;

import java.io.IOException;
import java.sql.Connection;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.doconcall.model.User;
import com.doconcall.service.UserService;
import com.doconcall.service.UserServiceImpl;
import com.mysql.jdbc.PreparedStatement;


@WebServlet("/changePass")
public class ChangePassword extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
    private UserService userService = new UserServiceImpl();
    
    public ChangePassword() {
        super();
        // TODO Auto-generated constructor stub
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String email = request.getParameter("email");
		User user = userService.getUserByEmail(email);
		request.setAttribute("user", user);
		request.getRequestDispatcher("/changePassword.jsp").forward(request, response);
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		User user = new User();
		user.setPassword(request.getParameter("password"));
		user.setId(Long.parseLong(request.getParameter("id")));
		user = userService.updateUser(user);
		if(user != null){
			request.setAttribute("message", "Your password is reset");
		}
		else{
			request.setAttribute("message", "Some error occurred");
		}
		request.getRequestDispatcher("/passwordReset.jsp").forward(request, response);
	}
}

	
		        	
		           

           
		             

     