package com.doconcall.web;

import java.io.IOException;
import java.net.HttpRetryException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.doconcall.dao.PatientDaoImpl;
import com.doconcall.model.Doctor;
import com.doconcall.model.Patient;
import com.doconcall.model.Schedule;
import com.doconcall.model.User;
import com.doconcall.service.DoctorService;
import com.doconcall.service.DoctorServiceImpl;
import com.doconcall.service.PatientService;
import com.doconcall.service.PatientServiceImpl;

/**
 * Servlet implementation class BookAnAppointment
 */
@WebServlet("/bookAnAppointment")
public class BookAnAppointment extends HttpServlet {
	private static final long serialVersionUID = 1L;
    private PatientService patientService=new PatientServiceImpl();   
    private DoctorService docservice = new DoctorServiceImpl();
    /**
     * @see HttpServlet#HttpServlet()
     */
    public BookAnAppointment() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		System.out.println("kajdvbfdmhkjgfakjgh");
		//response.getWriter().append("Served at: ").append(request.getContextPath()+request.getParameter("docId"));
		HttpSession session = request.getSession();
		Long docId = Long.parseLong(request.getParameter("docId"));
		
		User user = (User) session.getAttribute("user");
		List<Schedule> scheduleList = new ArrayList<>();	
		
		//System.out.println(user.getId());
		Patient patient=patientService.getById(user.getId());
		Doctor doctor = docservice.getById(docId);
		System.out.println(patient);
		//Patient patient=pdi.getById(user.getId());
		
		//session.setAttribute("patient", patient);
		
		
      //  System.out.println(patient.getFirstname());
      //  System.out.println(patient.getLastname());
        System.out.println(docId);
        
        scheduleList =docservice.getByScheduleId(docId);
       request.setAttribute("scheduleList",scheduleList);
       request.setAttribute("patient", patient);
       request.setAttribute("doctor", doctor);
		RequestDispatcher rd = request.getRequestDispatcher("appointments.jsp");
		rd.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
