package com.doconcall.web;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.doconcall.dao.UserAlreadyExistsException;
import com.doconcall.dao.UserDao;
import com.doconcall.dao.UserDaoImpl;
import com.doconcall.model.Patient;
import com.doconcall.model.User;
import com.doconcall.service.PatientService;
import com.doconcall.service.PatientServiceImpl;
import com.doconcall.service.UserService;
import com.doconcall.service.UserServiceImpl;
import com.doconcall.validator.UserValidator;

@WebServlet("/PatientServlet")
public class PatientServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private UserService userService = new UserServiceImpl();
    private PatientService patientService = new PatientServiceImpl();
	
	public PatientServlet() {
		super();

	}

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		String email = request.getParameter("email");
		String password = request.getParameter("password");
		String firstname = request.getParameter("firstname");
		String lastname = request.getParameter("lastname");
		String dob = request.getParameter("dob");
		String gender = request.getParameter("gender");
		
		User user = new User();
		user.setEmail(email);
		user.setPassword(password);
		
		UserValidator userValidator = UserValidator.getInstance();
		if(!userValidator.validateUser(user)){
			System.out.println("User is not valid");
		}
		else{
			System.out.println("User is valid");
		}
		
		try {
			user = userService.register(user);
			Patient patient = new Patient();
		
			patient.setUserId(user.getId());
			patient.setFirstname(firstname);
			patient.setLastname(lastname);
			patient.setDob(dob);
			patient.setGender(gender);
	
			patient =patientService.create(patient);
			
		    } 
		    
		
		     catch (UserAlreadyExistsException e) {
			System.out.println(e);
			request.setAttribute("emailAlreadyExistError",
					"An account with this email already exists !!");
			RequestDispatcher rd = request.getRequestDispatcher("SignUp.jsp");
			rd.forward(request, response);
		}
		request.setAttribute("user", user);
		RequestDispatcher rd = request.getRequestDispatcher("registerSuccessful.jsp");
		rd.forward(request, response);
	}
}
