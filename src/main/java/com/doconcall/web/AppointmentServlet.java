package com.doconcall.web;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.doconcall.dao.PatientDaoImpl;
import com.doconcall.model.Patient;
import com.doconcall.model.User;
import com.doconcall.service.PatientService;
import com.doconcall.service.PatientServiceImpl;

/**
 * Servlet implementation class AppointmentServlet
 */
@WebServlet("/AppointmentServlet")
public class AppointmentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	 private PatientService patientService = new PatientServiceImpl();

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AppointmentServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
		HttpSession session = request.getSession();

		User user = (User) session.getAttribute("user");
			
		PatientDaoImpl pdi=new PatientDaoImpl();
		System.out.println(user.getId());
		/*Patient patient=pdi.getById(user.getId());
		System.out.println(patient);*/
		Patient patient=pdi.getById(user.getId());
		
		session.setAttribute("patient", patient);
		
		
        System.out.println(patient.getFirstname());
        System.out.println(patient.getLastname());
        

		RequestDispatcher rd = request.getRequestDispatcher("appointments.jsp");
		rd.forward(request, response);

	}

	
	 
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub

		doGet(request, response);
	}

}
