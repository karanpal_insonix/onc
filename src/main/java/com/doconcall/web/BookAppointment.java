package com.doconcall.web;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.doconcall.model.Appointment;
import com.doconcall.model.Doctor;
import com.doconcall.model.Patient;
import com.doconcall.model.Schedule;
import com.doconcall.service.AppointmentServiceImpl;
import com.doconcall.service.DoctorService;
import com.doconcall.service.DoctorServiceImpl;
import com.doconcall.service.PatientService;
import com.doconcall.service.PatientServiceImpl;

/**
 * Servlet implementation class BookAppointment
 */
@WebServlet("/bookAppointment")
public class BookAppointment extends HttpServlet {
	private static final long serialVersionUID = 1L;
    private AppointmentServiceImpl appointmentService = new AppointmentServiceImpl();
    private DoctorService doctorService = new DoctorServiceImpl();
    private PatientService patientService = new PatientServiceImpl();
    /**
     * @see HttpServlet#HttpServlet()
     */
    public BookAppointment() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		String firstName = request.getParameter("firstName");
		String lastName = request.getParameter("lastName");
		String date = request.getParameter("date");
		String time = request.getParameter("time");
		String illness = request.getParameter("illness");
		String patientId = request.getParameter("patientId");
		String doctorId = request.getParameter("doctorId");
		
		Doctor doc = doctorService.getById(Long.parseLong(doctorId));
		Patient patient = patientService.get(Long.parseLong(patientId));
		Schedule schedule = doctorService.getScheduleById(Long.parseLong(time));
		
		Appointment appointment = new Appointment();
		appointment.setDoctor(doc);
		appointment.setPatient(patient);
		appointment.setSchedule(schedule);
		appointment.setReason(illness);
		schedule = doctorService.blockSchedule(schedule.getId());
		appointment = appointmentService.fixAppointment(appointment);
		System.out.println("Details "+appointment);
		
		request.setAttribute("appointment", appointment);
		request.getRequestDispatcher("appointmentBooked.jsp").forward(request, response);
		
	}

}
