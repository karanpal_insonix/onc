package com.doconcall.web;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.doconcall.model.Doctor;
import com.doconcall.service.DoctorService;
import com.doconcall.service.DoctorServiceImpl;

@WebServlet("/doctorRegistrationServlet")
public class DoctorRegistrationServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private DoctorService doctorService = new DoctorServiceImpl();

	public DoctorRegistrationServlet() {
		super();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String email = request.getParameter("email");
		String password = request.getParameter("password");
		String firstname = request.getParameter("firstname");
		String lastname = request.getParameter("lastname");
		String gender = request.getParameter("gender");
		String licenseNumber = request.getParameter("Liscence No");
		String speciality = request.getParameter("Speciality");
		String sector = request.getParameter("Sector");
		String city = request.getParameter("city");
		Doctor user = new Doctor();
		user.setEmail(email);
		user.setPassword(password);

		try {

			Doctor doctor = new Doctor();

			doctor.setUserId(user.getId());
			doctor.setFirstName(firstname);
			doctor.setLastName(lastname);

			doctor = doctorService.create(doctor);

		}

		catch (Exception e) {
			System.out.println(e);
			request.setAttribute("emailAlreadyExistError", "An account with this email already exists !!");
			RequestDispatcher rd = request.getRequestDispatcher("SignUp.jsp");
			rd.forward(request, response);
		}
		request.setAttribute("doctor", user);
		RequestDispatcher rd = request.getRequestDispatcher("registerSuccessful.jsp");
		rd.forward(request, response);
	}

}
