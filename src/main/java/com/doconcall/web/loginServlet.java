package com.doconcall.web;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.doconcall.model.User;
import com.doconcall.service.UserService;
import com.doconcall.service.UserServiceImpl;


@WebServlet("/login")
public class loginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	 private UserService userService = new UserServiceImpl();
       
    
    public loginServlet() {
        super();
        
    }

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String email = request.getParameter("email");
		if(email.indexOf("@") < 0){
			System.out.println("Invalid email");
		}
		String password = request.getParameter("password");
		if(email == null || email.length() < 3 || password == null || password.length() < 1){
			response.getWriter().append("Please provide correct details");
		}
		User user = userService.authenticate(email, password);
		System.out.println("User user "+user);
		if(user != null) {
			//response.getWriter().append("Welcome "+user.getEmail());
			HttpSession session = request.getSession();
			session.setAttribute("user", user);
			System.out.println(user);
			response.sendRedirect("index.jsp");
		}
		else {
			request.setAttribute("error","Invalid Username or Password !!");
			RequestDispatcher rd=request.getRequestDispatcher("index.jsp");  
			rd.forward(request, response);
					
			
		}
		
	}

}
