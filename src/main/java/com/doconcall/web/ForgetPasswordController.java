package com.doconcall.web;

import java.io.IOException;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.doconcall.model.User;
import com.doconcall.service.UserService;
import com.doconcall.service.UserServiceImpl;

@WebServlet("/forgetPassword")
public class ForgetPasswordController extends HttpServlet{
	
	private final static String username = "doconcall007@gmail.com";
	private final static String password = "java@123";
	private UserService userService = new UserServiceImpl();
	
	private static final long serialVersionUID = 5865310424151277105L;
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		RequestDispatcher rd=request.getRequestDispatcher("forgetPassword.jsp");  
		rd.forward(request, response);
	}
	
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		System.out.print("post");
		User user = null;
		String email = request.getParameter("email");
		 user = userService.getUserByEmail(email);
		 System.out.print(user);
		System.out.println(user==null);
		if(user == null){
			request.setAttribute("errorMessage", "Email not found. Please try again");
			RequestDispatcher rd=request.getRequestDispatcher("forgetPassword.jsp");  
			rd.forward(request, response);
		}
		else{
			System.out.println("Request URL "+request.getRequestURL()+","+request.getContextPath());
		
		
		
	
		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.port", "587");

	Session session = Session.getInstance(props,
				  new javax.mail.Authenticator() {
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication(username, password);
					}
				  });

		try {

			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress("doconcall007@gmail.com"));
			message.setRecipients(Message.RecipientType.TO,
				InternetAddress.parse(user.getEmail()));
			message.setSubject("Reset Your password");
			message.setText("http://localhost:8080/doconcall/changePass?email="+user.getEmail());

			Transport.send(message);

			System.out.println("Done::"+"http://localhost:8080/doconcall/changePassword?email="+user.getEmail());

		} catch (MessagingException e) {
			throw new RuntimeException(e);
		
		}
		
			request.setAttribute("message","Email sent");
			RequestDispatcher rd=request.getRequestDispatcher("forgetPassword.jsp");  
			rd.forward(request, response);
				
	    //RequestDispatcher rd=request.getRequestDispatcher("forgetPassword.jsp");  
		//rd.forward(request, response);
		
		
	}
	}

}
