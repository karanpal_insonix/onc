package com.doconcall.service;

import java.util.List;

import com.doconcall.model.Location;

public interface LocationService {
	public Location create(Location loc);
	public Location update(Location loc);
	public Location getById(Long id);
	public boolean delete(Long id);
	public List<Location> getAll();
}
