package com.doconcall.service;

import com.doconcall.dao.AppointmentDaoImpl;
import com.doconcall.model.Appointment;

public class AppointmentServiceImpl {
	private AppointmentDaoImpl appointmentDao = new AppointmentDaoImpl(); 
	public Appointment fixAppointment(Appointment appointment){
		return appointmentDao.saveAppointment(appointment);
	}
}
