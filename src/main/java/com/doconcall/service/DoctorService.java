package com.doconcall.service;

import java.util.List;

import com.doconcall.model.Doctor;
import com.doconcall.model.Location;
import com.doconcall.model.Schedule;

public interface DoctorService {
public Doctor create(Doctor doctor);
public Doctor  update(Doctor doctor);
public Doctor getById( Long id);
public boolean delete(Long id);
/*public List<Doctor> getAllDoctors();*/
public List<Doctor> getAll();
List<Doctor> getAllDoctors();
public List<Doctor> getSpecialityIdAndlocationId(Long locationId,Long specialityId);
public List<Schedule> getByScheduleId(Long id);

public Schedule getScheduleById(Long id);

public Schedule blockSchedule(Long id);

}


