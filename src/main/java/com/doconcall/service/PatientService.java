package com.doconcall.service;

import java.util.List;

import com.doconcall.model.Patient;

public interface PatientService {
	public Patient create(Patient patient);
	public Patient update(Patient patient);
	public Patient getById(Long id );
	public Patient delete(Long id);
	public List<Patient> getAll();
	public Patient get(Long id);
}
