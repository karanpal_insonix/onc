package com.doconcall.service;

import com.doconcall.dao.UserAlreadyExistsException;
import com.doconcall.model.User;


	public interface UserService {
		public User register(User user) throws UserAlreadyExistsException;
		public User create(User user);
		public User updateUser(User user);
		public void delete(Long id);
		public User find(Long id);
		public User authenticate(String email, String password);
		public User getUserByEmail(String email);

	}



