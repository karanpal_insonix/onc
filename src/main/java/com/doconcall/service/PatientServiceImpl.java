package com.doconcall.service;

import java.util.List;

import com.doconcall.dao.PatientDao;
import com.doconcall.dao.PatientDaoImpl;
import com.doconcall.model.Patient;

public class PatientServiceImpl implements PatientService{
	private PatientDao patientDao = new PatientDaoImpl();

	@Override
	public Patient create(Patient patient) {
		
		return patientDao.create(patient);
		 
	}
	
	

	@Override
	public Patient get(Long id) {
		return patientDao.get(id);
	}



	@Override
	public Patient update(Patient patient) {
		
		return patientDao.update(patient);
	}

	@Override
	public Patient getById(Long id) {
		
		return patientDao.getById(id);
	}

	@Override
	public Patient delete(Long id) {
		
		return null;
	}

	@Override
	public List<Patient> getAll() {
		
		return null;
	}

}
