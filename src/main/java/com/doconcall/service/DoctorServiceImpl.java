package com.doconcall.service;

import java.util.List;

import com.doconcall.dao.DoctorDao;
import com.doconcall.dao.DoctorDaoImpl;
import com.doconcall.model.Doctor;
import com.doconcall.model.Schedule;

public class DoctorServiceImpl implements DoctorService {
	private DoctorDao doctorDao = new DoctorDaoImpl();

	@Override
	public Doctor create(Doctor doctor) {
		// TODO Auto-generated method stub
		return doctorDao.create(doctor);
	}

	@Override
	public Doctor update(Doctor doctor) {
		// TODO Auto-generated method stub
return doctorDao.update(doctor);
	}

	@Override
	public Doctor getById(Long id) {
		// TODO Auto-generated method stub
		return doctorDao.getById(id);
	}

	@Override
	public boolean delete(Long id) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public List<Doctor> getAllDoctors() {
		
		// TODO Auto-generated method stub
		return doctorDao.getAll();
	}

	@Override
	public List<Doctor> getAll() {
		// TODO Auto-generated method stub
		return doctorDao.getAll();
	}

	@Override
	public List<Doctor> getSpecialityIdAndlocationId(Long locationId,Long specialityId) {
		// TODO Auto-generated method stub
		
		return doctorDao.getSpecialityIdAndlocationId(locationId,specialityId);
	}

	@Override
	public List<Schedule> getByScheduleId(Long id) {
		// TODO Auto-generated method stub
		System.out.println(id);
		return doctorDao.getByScheduleId(id);
	}

	@Override
	public Schedule getScheduleById(Long id) {
		return doctorDao.getScheduleById(id);
	}

	@Override
	public Schedule blockSchedule(Long id) {
		return doctorDao.blockSchedule(id);
	}
	
	
	
	
	
}
