package com.doconcall.service;

import com.doconcall.dao.UserAlreadyExistsException;
import com.doconcall.dao.UserDao;
import com.doconcall.dao.UserDaoImpl;
import com.doconcall.model.User;

public class UserServiceImpl implements UserService {
	private UserDao userDao = new UserDaoImpl();
	
	public User create(User user) {
		return null;
	}

	public User updateUser(User user) {
		return userDao.update(user);
	}

	public void delete(Long id) {
	}

	public User find(Long id) {
		return null;
	}

	public User authenticate(String email, String password) {
		return userDao.authenticate(email, password);
	}

	@Override
	public User register(User user) throws UserAlreadyExistsException {
		return userDao.register(user);
	}

	@Override
	public User getUserByEmail(String email) {
		return userDao.getUserByEmail(email);
	}

}
