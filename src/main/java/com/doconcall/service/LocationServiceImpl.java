package com.doconcall.service;

import java.util.List;

import com.doconcall.dao.LocationDao;
import com.doconcall.dao.LocationDaoImpl;
import com.doconcall.model.Location;

public class LocationServiceImpl implements LocationService {
	private LocationDao locationDao = new LocationDaoImpl();
	@Override
	public Location create(Location loc) {
		return null;
	}

	@Override
	public Location update(Location loc) {
		return null;
	}

	@Override
	public Location getById(Long id) {
		return null;
	}

	@Override
	public boolean delete(Long id) {
		return false;
	}

	@Override
	public List<Location> getAll() {
		return locationDao.getAll();
	}

}
