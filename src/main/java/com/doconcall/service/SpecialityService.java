package com.doconcall.service;

import java.util.List;

import com.doconcall.model.Speciality;

public interface SpecialityService {

public Speciality create(Speciality spe);
	
	public List<Speciality> getAll();

}
