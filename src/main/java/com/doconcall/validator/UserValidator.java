package com.doconcall.validator;



import org.apache.commons.validator.routines.EmailValidator;

import com.doconcall.model.User;

public class UserValidator {
	
	private static final UserValidator USER_VALIDATOR = new UserValidator();
	
	private UserValidator(){
		
	}
	public static UserValidator getInstance(){
		return USER_VALIDATOR;			
	}
	
	public boolean validateUser(User user){
		EmailValidator emailValidator = EmailValidator.getInstance(false);
		if(!emailValidator.isValid(user.getEmail())){
			return false;
		}
		String password = user.getPasword().trim();
		if(password.length() < 6 || password.length() > 16){
			return false;
		}
		return true;
	}
	
	
}
