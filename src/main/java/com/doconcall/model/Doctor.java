package com.doconcall.model;

import java.util.List;

public class Doctor extends User {
	private Long id;
	private String firstName;
	private String lastName;
	private String licenseNumber;
	private Long userId;
	private Speciality speciality;
	private Location location;
	private Long specialityId;
	private Long locationId;
	private String clinicName;
	private Long pinNo;
	private String city;
	private String sector;
 
	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstName() {
		
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getLicenseNumber() {
		return licenseNumber;
	}

	public void setLicenseNumber(String licenseNumber) {
		this.licenseNumber = licenseNumber;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}
	
	

	public Long getSpecialityId() {
		return specialityId;
	}

	public void setSpecialityId(Long specialityId) {
		this.specialityId = specialityId;
	}

	public Long getLocationId() {
		return locationId;
	}

	public void setLocationId(Long locationId) {
		this.locationId = locationId;
	}

	public Speciality getSpeciality() {
		return speciality;
	}

	public void setSpeciality(Speciality speciality) {
		this.speciality = speciality;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	


	@Override
	public String toString() {
		return "Doctor [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + ", licenseNumber="
				+ licenseNumber + ", userId=" + userId + ", speciality=" + speciality + ", location=" + location
				+ ", specialityId=" + specialityId + ", locationId=" + locationId + ", clinicName=" + clinicName
				+ ", pinNo=" + pinNo + ", city=" + city + ", sector=" + sector + "]";
	}

	public String getClinicName() {
		return clinicName;
	}

	public void setClinicName(String clinicName) {
		this.clinicName = clinicName;
	}

	public Long getPinNo() {
		return pinNo;
	}

	public void setPinNo(Long pinNo) {
		this.pinNo = pinNo;
	}

	public String getSector() {
		return sector;
	}

	public void setSector(String sector) {
		this.sector = sector;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	
	}

	
