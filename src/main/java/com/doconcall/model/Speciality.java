package com.doconcall.model;

public class Speciality {

	private Long id;
	private String name;
	
	
	public Speciality() 
	{
		super();
	}
	public Speciality(Long id, String name) {
		super();
		this.id= id;
		this.name = name;	
	}
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	
	@Override
	public String toString() {
		return "Speciality [id=" + id + ", name=" + name + "]";
	}
	
}
