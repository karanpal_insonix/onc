package com.doconcall.model;

public class Schedule {
	private Long id;
	private String date;
	private String time;
	private String filled;
	
	
	
	public Schedule() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	public Schedule(Long id, String date, String time, String filled) {
		super();
		this.id = id;
		this.date = date;
		this.time = time;
		this.filled = filled;
	}


	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public String getFilled() {
		return filled;
	}
	public void setFilled(String filled) {
		this.filled = filled;
	}


	@Override
	public String toString() {
		return "Schedule [id=" + id + ", date=" + date + ", time=" + time + ", filled=" + filled + "]";
	}
	
	
}
