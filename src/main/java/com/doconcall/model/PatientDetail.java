package com.doconcall.model;

public class PatientDetail 
{
	private String id;
	private String illness;
	
	public String getId() 
	{
		return id;
	}
	public void setId(String id) 
	{
		this.id = id;
	}
	public String getIllness() 
	{
		return illness;
	}
	public void setIllness(String illness) 
	{
		this.illness = illness;
	}
	
	@Override
	public String toString() 
	{
		return "PatientDetail [id=" + id + ", illness=" + illness + "]";
	}
	
	
}
