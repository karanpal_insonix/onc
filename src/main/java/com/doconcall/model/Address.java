package com.doconcall.model;

public class Address extends User 
{
	private Long id;
	private String hno;
	private String streetno;
	private String city;
	private String type;
	
	public Long getId() 
	{
		return id;
	}
	public void setId(Long id) 
	{
		this.id = id;
	}
	public String getHno() 
	{
		return hno;
	}
	public void setHno(String hno) 
	{
		this.hno = hno;
	}
	public String getStreetno() 
	{
		return streetno;
	}
	public void setStreetno(String streetno) 
	{
		this.streetno = streetno;
	}
	public String getCity() 
	{
		return city;
	}
	public void setCity(String city) 
	{
		this.city = city;
	}
	public String getType() 
	{
		return type;
	}
	public void setType(String type) 
	{
		this.type = type;
	}
	
	@Override
	public String toString() 
	{
		return "Address [id=" + id + ", hno=" + hno + ", streetno=" + streetno + ", city=" + city + ", type=" + type
				+ "]";
	}

	
	
}
