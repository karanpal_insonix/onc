package com.doconcall.model;

public class Location {
	private Long id;
	private String city;
	private String sector;
	private String clinicName;
	private Long pinNo;
	private Long doctorId;
	
	
	
	public Location() {
		super();
	}
	
	public Location(Long id, String city, String sector, String clinicName, Long pinNo,Long doctorId  ) {
		super();
		this.id = id;
		this.city = city;
		this.sector = sector;
		this.clinicName = clinicName;
		this.pinNo = pinNo;
		this.doctorId = doctorId;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getSector() {
		return sector;
	}
	public void setSector(String sector) {
		this.sector = sector;
	}
	

	public String getClinicName() {
		return clinicName;
	}

	public void setClinicName(String clinicName) {
		this.clinicName = clinicName;
	}

	public Long getPinNo() {
		return pinNo;
	}

	public void setPinNo(Long pinNo) {
		this.pinNo = pinNo;
	}

	public Long getDoctorId() {
		return doctorId;
	}

	public void setDoctorId(Long doctorId) {
		this.doctorId = doctorId;
	}
	
	@Override
	public String toString() {
		return "Location [id=" + id + ", city=" + city + ", sector=" + sector + " ClinicName="+ clinicName +" PinNo=" +pinNo+ " ]";
	}
	
	
}
